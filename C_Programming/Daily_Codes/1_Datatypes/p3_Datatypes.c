/* Datatypes and their size*/

#include<stdio.h>

void main()
{
	int age = 19;
	float petPrice = 105.85;
	double goldPrice = 48000.1234506;
	char wing = 'A';

	//void v;
	
	printf("%d \n", age);
	printf("%f \n", petPrice);
	printf("%lf \n", goldPrice);
	printf("%c \n", wing);


	printf("%ld \n", sizeof(int));
	printf("%ld \n", sizeof(float));
	printf("%ld \n", sizeof(double));
	printf("%ld \n", sizeof(char));
	printf("%ld \n", sizeof(void));

}

