/*      
Methods in string

    Length() ::  It returns the number of characters contained in given string.
*/


class StringLength{
    public static void main(String[] args) {
        
        System.out.println();

        String str1 = "core2web";
        System.out.println("Length of string::  " + str1.length());

        System.out.println();
    }
}
