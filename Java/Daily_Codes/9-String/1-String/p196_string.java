//identityHashCode of string


class StringDemo{

    public static void main(String[] args) {

        System.out.println();
        
        String str1 = "core2web";
        String str2 = new String("core2web");

        System.out.println("str1 ::   " + System.identityHashCode(str1));
        System.out.println("str2 ::   " +System.identityHashCode(str2));

        String str3 = "core2web";
        String str4 = new String("core2web");

        System.out.println("str3 ::   " +System.identityHashCode(str3));
        System.out.println("str4 ::   " +System.identityHashCode(str4));

        System.out.println();

    }
}