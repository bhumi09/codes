/*      
Methods in string

    Concat() ::  Concatinate String to this string i.e another string is concatinated with the first string.
                 Implements new Array of character whose length is sum of str1.length and str2.length 

*/


class solution{

    String myStrConcat(String str1, String str2){

        return str1 + str2;
    }
    public static void main(String[] args) {
        
        String str1 = "Bhumika";
        String str2 = "Shejwal";

        solution obj = new solution();

        System.out.println("Concatenated string::  " + obj.myStrConcat(str1, str2));

    }
}