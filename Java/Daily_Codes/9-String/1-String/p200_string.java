//Trial and run code

import javax.annotation.processing.SupportedOptions;

class StringDemo{
    public static void main(String[] args) {

        System.out.println();
        
        String str1 = "Bhumika";
        String str2 = "Shejwal";

        System.out.println(str1 + str2);

        String str3 = "BhumikaShejwal";
        String str4 = str1 + str2;              //Here strings are concatenated. Therefore str4 gives call to new String and it returns the address of new string.

        System.out.println("str3 ::   " + System.identityHashCode(str3));
        System.out.println("str4 ::   " + System.identityHashCode(str4));

        System.out.println();
    }
}