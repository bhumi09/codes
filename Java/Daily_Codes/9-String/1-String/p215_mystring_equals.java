//Comparing content of string without using equals()

import java.io.*;
class solution{

    int myStrLength(String str){

        int cnt = 0;
        char arr[] = str.toCharArray();

        for(int i = 0; i< arr.length; i++){
            cnt++;
        }
        return cnt;
    }

    char myCharAt(String str, int index){

        char arr[] = str.toCharArray();
        return arr[index];
    }

    boolean myStringEquals(String str1,  String str2){

        int length = myStrLength(str1);

        for(int i = 0; i < length; i++){

            char ch = myCharAt(str1, i);
            char ch2 = myCharAt(str2, i);

            if(ch != ch2){
                return false;
                }
            }
            return true;
        }
    
    public static void main(String[] args)throws IOException {

	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        solution s1 = new solution();

	System.out.print("\nEnter the first string::  ");
        String str1 = br.readLine();

	System.out.print("\nEnter the second string::  ");
        String str2 = br.readLine();

        boolean retVal = s1.myStringEquals(str1, str2);

        if(retVal == true){
            System.out.println("\n Strings are equal...");
        }
        else{
            System.out.println("\n String are not equal..");
        }
    }
}

