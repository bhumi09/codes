/*
  toCharArray() ::  It converts the string into character array.
 */

 class solution{
    public static void main(String[] args) {
        
        String str1 = "BhumikaShejwal";

        char arr[] = str1.toCharArray();

        for(int i = 0; i< arr.length; i++){
            
            System.out.println("   " + arr[i]);
        }
    }
 }