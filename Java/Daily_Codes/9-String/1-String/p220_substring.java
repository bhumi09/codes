/*      
Methods in string

    substring() ::  Creates a substring of the given string starting at a specified index 
                    and ending at the end of given string. 
*/

class solution{
    public static void main(String[] args) {
        
        String str1 = "Core2web Tech";

        System.out.println(str1.substring(5));
        System.out.println(str1.substring(0, 3));

    }
}