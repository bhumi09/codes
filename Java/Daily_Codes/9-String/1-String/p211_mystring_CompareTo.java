/*      
Comapring two strings without using compareTo()
*/
import java.io.*;
class solution{

    int myStrLength(String str){

        int cnt = 0;
        char arr[] = str.toCharArray();

        for(int i = 0; i< arr.length; i++){
            cnt++;
        }
        return cnt;
    }

    char myCharAt(String str, int index){

        char arr[] = str.toCharArray();
        return arr[index];
    }

    int myCompareTo(String str1,  String str2){

        int length = myStrLength(str1);

        for(int i = 0; i < length; i++){

            char ch = myCharAt(str1, i);
            char ch2 = myCharAt(str2, i);

            if(ch != ch2){
                return ch - ch2;
            }
        }
        return 0;
    }
    public static void main(String[] args)throws IOException {

	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        solution s1 = new solution();

	System.out.print("\nEnter the first string::  ");
        String str1 = br.readLine();

	System.out.print("\nEnter the second string::  ");
        String str2 = br.readLine();

        int str1_length = s1.myStrLength(str1);
        int str2_length = s1.myStrLength(str2);

        if(str1_length != str2_length){

            System.out.println("\n Strings have different length by " + (str1_length - str2_length));
        }
        else{
            int str_diff = s1.myCompareTo(str1, str2);

            if(str_diff == 0){
                System.out.println("\nStrings are equal....\n");
            }
            else{
                System.out.println("\nString are not equal with difference of::  " + str_diff);
            }
        }
        
    }
}
