/*      
Methods in string

    replace() ::  Finds last instance of the character in the given string.
*/

class solution{
    public static void main(String[] args) {
        
        String str1 = "Bhumika";

        System.out.println("\n Original string::   " + str1);
        System.out.println("Replace character string::   " + str1.replace('h', 'a') + "\n");
       
    }
}