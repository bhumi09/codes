//Trial and error code
//Concanitating two strings

class StringDemo{
    public static void main(String[] args) {
        
        System.out.println();

        String str1 = "Bhumika";
        String str3 = "Shejwal";

        String str2 = str1 + str3;
        String str4 = str1.concat(str3);

        System.out.println("Str2 ::   " + str2);
        System.out.println("Str4 ::   " + str4);

        System.out.println();
    }
}