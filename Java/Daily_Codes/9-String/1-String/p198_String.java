
//  Trial and run codes

class StringDemo{
    public static void main(String[] args) {

        System.out.println();

        String str1 = "kanha";
        String str2 = "kanha";

        String str3 = new String("kanha");
        String str4 = new String("kanha");

        String str5 = new String("rahul");
        String str6 = "rahul";

        System.out.println("Str1 ::   " + System.identityHashCode(str1));
        System.out.println("Str2 ::   " + System.identityHashCode(str2));

        System.out.println("Str3 ::   " + System.identityHashCode(str3));
        System.out.println("Str4 ::   " + System.identityHashCode(str4));

        System.out.println("Str5 ::   " + System.identityHashCode(str5));
        System.out.println("Str6 ::   " + System.identityHashCode(str6));

        System.out.println();
        
    }
}