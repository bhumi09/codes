//Trial and run codes

class StringDemo {

    public static void main(String[] args) {

        System.out.println();

        String str1 = "kanha";
        String str2 = str1;
        String str3 = new String(str2);

        System.out.println("str1 ::   " + System.identityHashCode(str1));
        System.out.println("str2 ::   " + System.identityHashCode(str2));
        System.out.println("str3 ::   " + System.identityHashCode(str3));

        System.out.println();
    }
}