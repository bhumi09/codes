/*      
Methods in string

    Concat() ::  Concatinate String to this string i.e another string is concatinated with the first string.
                 Implements new Array of character whose length is sum of str1.length and str2.length 

*/

class ConcatDemo{
    public static void main(String[] args) {
        
        System.out.println();

        String str1 = "core2";
        String str2 = "web";

        String str3 = str1.concat(str2);
        System.out.println(str3 + "\n");

/* 
//Concatinating string using Command line argument
        String str1 = args[0];
        String str2 = args[1];

        String str3 = str1.concat(str2);
        System.out.println(str3 + "\n");

    */

    }
}