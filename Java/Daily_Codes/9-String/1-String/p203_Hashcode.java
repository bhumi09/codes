//Hashcode

class StringDemo{
    public static void main(String[] args) {
        
        System.out.println();

        String str1 = "Bhumika";
        String str2 = new String("Bhumika");

        String str3 = "Bhumika";
        String str4 = new String("Bhumika");

        System.out.println("str1 ::   " + str1.hashCode());
        System.out.println("str2 ::   " + str2.hashCode());
        System.out.println("str3 ::   " + str3.hashCode());
        System.out.println("str4 ::   " + str4.hashCode());

        System.out.println();

        String str5 =  "Shashi";
        System.out.println("str5 ::   " + str5.hashCode());



    }
}