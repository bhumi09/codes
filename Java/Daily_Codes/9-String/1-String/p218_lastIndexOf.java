/*      
Methods in string

    lastIndexOf() ::  Finds last instance of the character in the given string.
*/

class solution{
    public static void main(String[] args) {
        
        String str1 = "Bhumika";

        System.out.println(str1.lastIndexOf('h', 5));
        System.out.println(str1.lastIndexOf('m', 4));
        System.out.println(str1.lastIndexOf('B', 5));
    }
}