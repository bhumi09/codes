/*      
Methods in string

    indexOf() ::  Finds first instance of the character in the given string.
*/

class solution{
    public static void main(String[] args) {
        
        String str1 = "Bhumika";

        System.out.println(str1.indexOf('h', 0));
        System.out.println(str1.indexOf('m', 1));
        System.out.println(str1.indexOf('B', 0));
    }
}