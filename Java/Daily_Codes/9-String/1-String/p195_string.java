//Declarations of string

class StringDemo{

    public static void main(String[] args) {
        
        System.out.println();

        String str1 = "core2web";
        String str2 = new String("core2web");

        char str3[] = {'c', '2', 'w'};

        System.out.println("str1 ::   " + str1);
        System.out.println("str2 ::   " + str2);
        System.out.println(str3);

        System.out.println();
        
    }
}