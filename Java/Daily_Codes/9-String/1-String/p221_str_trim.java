/*      
Methods in string

    trim() ::  Trims all the white spaces before and after of the string.  
*/

class solution{
    public static void main(String[] args) {
        
        String str = " Know the code till the core ";
        System.out.println(str.trim());
    }
}