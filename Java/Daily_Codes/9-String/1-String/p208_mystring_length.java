/*
  Length of string without using length()
 */

class solution {

    int myStrLength(String str) {

        int cnt = 0;

        char arr[] = str.toCharArray();

        for (int i = 0; i < arr.length; i++) {

            cnt++;
        }
        return cnt;
    }

    public static void main(String[] args) {

        String str = "Bhumika";

        solution obj = new solution();

        System.out.println("Length of string::  " + obj.myStrLength(str));

    }
}