/* 
Returning character of specified index without using charAt()
*/

class solution{

    char myCharAt(String str, int index){

        char ch[] = str.toCharArray();
        return ch[index];
    }
    public static void main(String[] args) {
        
        System.out.println();
        solution s1 = new solution();

        String str = "Bhumika";
        System.out.println(s1.myCharAt(str, 3));

        System.out.println();
    }
}