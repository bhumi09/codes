/*      
Methods in string

    split() ::  Splits this string around matches of regular expressions.
*/

class solution{
    public static void main(String[] args) {

        System.out.println();
        
        String str = " Know the code till the core ";
        String[] strResult = str.split("");

        for(int i = 0; i< strResult.length; i++){
            
            System.out.println(strResult[i]);
        }
        System.out.println();
    }
}