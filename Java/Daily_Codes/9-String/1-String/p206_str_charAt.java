/*      
Methods in string

    charAt() ::  It returns the character located at specified index within the given string.

*/

class StringCharAt {
    public static void main(String[] args) {

        String str = "core2web";

        System.out.println(str.charAt(4));
        System.out.println(str.charAt(0));
        System.out.println(str.charAt(8)); // Exception in thread "main" java.lang.StringIndexOutOfBoundsException:
                                           // String index out of range: 8

        /*
    //CharAt method using on Command line argument
        
        String str2 = args[0];
        System.out.println(str2.charAt(4));
        System.out.println(str2.charAt(0));
         
         */

    }
}