//Character array in string

class StringDemo{
    public static void main(String[] args) {

        System.out.println();

        char arr[] = {'A', 'B', 'C'};

        System.out.println(arr.toString());
        System.out.println("Array::  " + arr);
        System.out.println(arr);
        
        System.out.println();
    }
}