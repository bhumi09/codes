/*
    Method::
    Public synchronized StringBuffer insert(int Offset, String str);
   
    Description::   Insert the <code> String </code> to this <code> StringBuffer </code>
                    If str is null, the String "null" is used instead
 */

 class solution{
    public static void main(String[] args) {

        System.out.println();
        
        StringBuffer sb = new StringBuffer("Bhumika Shejwal");
        System.out.println(sb);

        sb.insert(7, " Kailas");
        System.out.println(sb);

        System.out.println();
    }
 }
