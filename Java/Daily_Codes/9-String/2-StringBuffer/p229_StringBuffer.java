//Tryrun code

class solution{
    public static void main(String[] args) {
        
        System.out.println();

        String str1 = "Bhumika";
        String str2 = new String("Kailas");
        StringBuffer str3 = new StringBuffer("Shejwal");      

        //String str4 = str1.concat(str3);            //error: incompatible types: StringBuffer cannot be converted to String
        StringBuffer str5 = str3.append(str2);

        System.out.println("String1::  " + str1);
        System.out.println("String2::  " + str2);
        System.out.println("String3::  " + str3);
        //System.out.println("String4::  " + str4);
        System.out.println("String5::  " + str5);

        System.out.println();
    }
}
