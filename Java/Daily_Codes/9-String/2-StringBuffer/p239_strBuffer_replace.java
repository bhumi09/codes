/*
    Method::
    public synchronized int replace(int start, int end, String str);
   
    Description::  Replace characters between index <code> start </code> (inclusive) <code> end </code>
                    (exclusive) with <code> str </code>.
 */

 class solution{
    public static void main(String[] args) {
        
        System.out.println();
        StringBuffer str1 = new StringBuffer("Know the code untill the core");
        System.out.println("String::  " + str1);

        str1.replace(14, 20, "till");
        System.out.println("Replace string::  "+ str1);
        System.out.println();
    }
 }