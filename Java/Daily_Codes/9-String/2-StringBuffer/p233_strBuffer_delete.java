/*
    Method::
    Public synchronized StringBuffer delete(int start, int end);
   
    Description::   delete characters from this <code> StringBuffer </code>
                    If is harmless for the end to be larger than length()
 */

 class solution{
    public static void main(String[] args) {

        System.out.println();
        StringBuffer sb = new StringBuffer("Bhumikaaa");

        sb.delete(5, 9);
        System.out.println(sb);
        System.out.println();
    }
 }