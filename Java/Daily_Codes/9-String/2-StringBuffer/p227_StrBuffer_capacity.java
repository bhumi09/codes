//StringBuffer capacity

class solution{
    public static void main(String[] args) {
        
        System.out.println();
        
        StringBuffer sb = new StringBuffer(100);

        sb.append("Biencaps");
        sb.append("Core2web");

        System.out.println("String::  " + sb);
        System.out.println("Capacity:: " + sb.capacity());
        System.out.println();

        sb.append("Incubator");

        System.out.println("String::  " + sb);
        System.out.println("Capacity:: " + sb.capacity());

        System.out.println();
    }
}