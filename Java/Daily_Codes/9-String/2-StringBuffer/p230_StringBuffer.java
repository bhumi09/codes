//Tryrun Codes

class solution{
    public static void main(String[] args) {

        System.out.println();
        
        String str1 = "Bhumika";
        String str2 = new String("Kailas");
        StringBuffer str3 = new StringBuffer("Shejwal");

        str1.concat(str2);
        str3.append(str2);

        System.out.println("String1::  " + str1);
        System.out.println("String2::  " + str2);
        System.out.println("String3::  " + str3);

        System.out.println();
    }
}