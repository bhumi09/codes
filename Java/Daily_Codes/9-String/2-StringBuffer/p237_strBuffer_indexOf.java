/*
    Method::
    public synchronized int indexOf(String str, int fromIndex);
   
    Description::  Finds first instance of a string in this StringBuffer starting at given index.
                    If the starting index is less than 0,the search starts at begining of this string.
                    If the starting index is greater than the length of this string, or the substring is not found,
                    then -1 is returned.  
 */

 class solution{
    public static void main(String[] args) {
        
        StringBuffer str1 = new StringBuffer("Bhumika");
        System.out.println("String found at index::   " + str1.indexOf("m"));
    }
 }