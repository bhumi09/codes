/*
    Method::
    Public synchronized StringBuffer append(String str);
   
    Description::   Append the <code> String </code> to this <code> StringBuffer </code>
                    If str is null, the String "null" is appended.
 */

 class solution{
    public static void main(String[] args) {

        System.out.println();
        
        StringBuffer str1 = new StringBuffer("Bhumika");
        String str2 = "Shejwal";

        str1.append(str2);

        System.out.println("Appended string::  " + str1);
        System.out.println();
    }
 }