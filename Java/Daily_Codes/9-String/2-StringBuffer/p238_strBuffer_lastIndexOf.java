/*
    Method::
    public synchronized int lastIndexOf(String str, int fromIndex);
   
    Description::  Finds last instance of a string in this StringBuffer starting at given index.
                    If the starting index is greater than maximum valid index,the search begins at 
                    end of this string..
                    If the starting index is less than than 0, or the substring is not found,
                    then -1 is returned.  
 */

 class solution{
    public static void main(String[] args) {
        
        StringBuffer str1 = new StringBuffer("Bhumika Shejwal");
        System.out.println("String found at index::   " + str1.lastIndexOf("h"));
    }
 }