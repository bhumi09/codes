/*
    Method::
    public synchronized int length();
   
    Description::  Get the elength of the <code> String </code> this a  <code>StringBuffer </code> would create.
                    The string is composed of the characters currently in this StringBuffer
 */

 class solution{
    public static void main(String[] args) {
        
        StringBuffer str1 = new StringBuffer("Bhumika");
        System.out.println("Length of " + str1 + " ::  " + str1.length());
    }
 }
