/*
    Method::
    Public synchronized StringBuffer reverse();
   
    Description::   Reverse the characters in this StringBuffer.
                    The same sequence of characters exists, but in the reverse index ordering.
 */

 class solution{
    public static void main(String[] args) {
        
        System.out.println();

        //First way
        StringBuffer sb = new StringBuffer("Bhumika");
        System.out.println("Reverse string::  " + sb);
        System.out.println();
        
        //Second way
        String str1 = "Shejwal";
        StringBuffer sb2 = new StringBuffer(str1);
        
        //Method chaining
        str1 = sb2.reverse().toString();
        System.out.println("Reverse string:: " + str1);
        System.out.println();
    }
 }

