/*
    Method::
    public String toString();
   
    Description::  Convert this <code> StringBuffer </code> to a <code>String </code>.
                    The string is composed of the characters currently in this StringBuffer
 */

 class solution{
    public static void main(String[] args) {

        System.out.println();
        
        StringBuffer str1 = new StringBuffer("Know the code till the core");
        String str2 = "Core2web ";
        String str3 = str1.toString();
        String str4 = str2.concat(str3);

        System.out.println(str4);
        System.out.println();
    }
 }