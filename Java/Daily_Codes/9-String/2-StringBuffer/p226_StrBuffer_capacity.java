//StringBuffer capacity


class solution{
    public static void main(String[] args) {

        System.out.println();
        
        StringBuffer sb = new StringBuffer();

        System.out.println("Capacity::  " + sb.capacity());
        System.out.println(sb);

        sb.append("Bhumika");
        System.out.println("Capacity::  " + sb.capacity());
        System.out.println("String::  " + sb);
        System.out.println();

        sb.append("Shejwal");
        System.out.println("Capacity::  " + sb.capacity());
        System.out.println("String::  " + sb);
        System.out.println();

        sb.append("Pune");
        System.out.println("Capacity::  " + sb.capacity());
        System.out.println("String::  " + sb);
        System.out.println();

    }
}