//Tryrun code

class solution{
    public static void main(String[] args) {

        System.out.println();
        
        String str1 = "Bhumika";
        String str2 = "Shejwal";
       
        StringBuffer str3 = new StringBuffer("Nashik");
        StringBuffer str4 = str3.append(str1);
       
        //String str4  = str1.append(str3);       // ERROR- variable str4 is already defined in method main(String[] 
        // String str5 = str3.append(str1);           //ERROR- incompatible types: StringBuffer cannot be converted to String
        
        System.out.println("String1::  " + str1);
        System.out.println("String2::  " + str2);
        System.out.println("String3::  " + str3);
        System.out.println("String4::  " + str4);

        System.out.println();
    
    }
}