/*
    Method::
    public synchronized String substring(int beginIndex, int endIndex);
   
    Description::  Creates a substring of this StringBuffer, starting at a specified index and ending at
                    one character before a specified index.
 */

 class solution{
    public static void main(String[] args) {
         
        System.out.println();
        StringBuffer str1 = new StringBuffer("Know the code till the core");
        System.out.println("String::  " + str1);

        String str2 = str1.substring(14, 27);
        System.out.println("Substring::  " + str2);
        System.out.println();
    }
 }