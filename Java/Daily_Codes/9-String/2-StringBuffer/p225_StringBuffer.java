//StringBuffer

class solution{
    public static void main(String[] args) {


        System.out.println();
        
        StringBuffer str1 = new StringBuffer("Bhumika");

        System.out.println("Before append str1:: " + System.identityHashCode(str1));

        str1.append("Shejwal");
        System.out.println("Append string::  " + str1);

        System.out.println("After append str1:: " + System.identityHashCode(str1));

        //String Capacity
        System.out.println("String capacity::  " + str1.capacity() + "\n");
    }
}