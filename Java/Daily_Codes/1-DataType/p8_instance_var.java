/* Instance variable and local variable */

class CricPlayer{

	int x = 110;			//instance variable

	public static void main(String args[]){

		int y = 20;		//local variable

		System.out.println(x);  //error: non-static variable x cannot be referenced from a static context
		System.out.println(y);
	}
}
