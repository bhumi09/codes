/* datatype byte and addition of two byte numbers */

class Demo{

	public static void main(String[] args){

		//byte var1 = 18;
		//byte var2 = 18;

		short var1 = 18;
		short var2 = 18;

		System.out.println(var1);
		System.out.println(var2);

		var1 = var1 + var2;		//ERROR: error: incompatible types: possible lossy conversion from int to short
		
		
		System.out.println(var1);
		System.out.println(var2);
	}
}
