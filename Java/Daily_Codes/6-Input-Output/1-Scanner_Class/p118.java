/*
   Take a input from user about their name, age and salary and print it.
   To take input use Scanner class.
 */

import java.util.Scanner;

class solution{


    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        //string input
        System.out.print("Enter your name:: ");
        String name = obj.next();

        //integer input
        System.out.print("Enter your age::  ");
        int age = obj.nextInt();

        //float input
        System.out.print("Enter the salary:: ");
        float salary = obj.nextFloat();

        System.out.println();

        System.out.println("Name = " + name);
        System.out.println("Age = " + age);
        System.out.println("Salary = " + salary);

        System.out.println();
        
    }
}