//Taking user input through Scanner class

import java.util.Scanner;
class solution{


    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        //string input
        System.out.print("Enter your name:: ");
        String name = obj.next();

        //integer input
        System.out.print("Enter your age::  ");
        int age = obj.nextInt();

        System.out.println("\n Name = " + name);
        System.out.println("Age = " + age);

        System.out.println();
        
    }
}