//Taking input through BufferedReader class

import java.io.*;

class solution{
    public static void main(String[] args)throws IOException{
        
        InputStreamReader isr = new InputStreamReader(System.in);

        System.out.println();

        System.out.print("Enter the character::  ");
        char ch = (char)isr.read();
        System.out.println("Character = " + ch);

        System.out.println();

    }
}

//NOTE:: when we use character as a input then we have to typecast it.