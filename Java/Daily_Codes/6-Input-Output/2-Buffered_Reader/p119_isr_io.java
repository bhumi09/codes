
//Taking input through BufferedReader class

import java.io.*;

class solution{
    public static void main(String[] args) {
        
        InputStreamReader isr = new InputStreamReader(System.in);

        System.out.println("Enter the character::  ");
        char ch = (char)isr.read();         //error: unreported exception IOException; must be caught or declared to be thrown

    }
}

//NOTE:: when we use character as a input then we have to typecast it.