/*
    Write a program to print match info like Teams Name, Man of the mathch and No. of runs
    Take all the input on a single line and print the output on different lines.

        Print:: (Enter Teams Name   Man of the mathch   Runs   Grade   Average )

        Input::  KKRvsGT    Rinku Singh   48
        Output::  Teams Name = KKRvsGT
                  Man of the mathch = Rinku Singh
                  No. of runs = 48
                  Grade = A
                  Average = 40.60

 */

 //NOTE:: To split the input we can StringTokenizer class.

 import java.util.*;
 import java.io.*;

 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Enter Teams Name   Man of the match   No. of runs   Grade   Average");

            String info = br.readLine();

            StringTokenizer obj = new StringTokenizer(info , "   ");

            String TName = obj.nextToken();
            String MOM = obj.nextToken();
            int runs = Integer.parseInt(obj.nextToken());
            char grade = obj.nextToken().charAt(0);
            float avg = Float.parseFloat(obj.nextToken());

            System.out.println("Teams Name =  " + TName);
            System.out.println("Man of the mathch=  " + MOM);
            System.out.println("No. of runs =  " + runs);
            System.out.println("Grade=  " + grade);
            System.out.println("Average=  " + avg);

            System.out.println();

    }
 }