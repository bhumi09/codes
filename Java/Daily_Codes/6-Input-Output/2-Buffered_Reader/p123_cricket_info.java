//Taking input through BufferedReader class
/* 

    Write a program for cricket player and take input such as player name, 
    player jersy number and players average point. 
    
*/

import java.io.*;

class solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter player name::  ");
        String name = br.readLine();

        System.out.print("Enter Jersy no.:: ");
        int run = Integer.parseInt(br.readLine());

        System.out.print("Enter the average:: ");
        float avg = Float.parseFloat(br.readLine());

        System.out.println();

        System.out.println("Player Name  = " + name);
        System.out.println("Jersy No. = " + run);
        System.out.println("Average = " + avg);

        System.out.println();

    }
}
