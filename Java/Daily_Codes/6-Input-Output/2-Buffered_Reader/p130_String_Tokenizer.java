//Take number of tokens from user

import java.util.*;

class solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter player info::  ");

        String str = sc.nextLine();
        StringTokenizer st = new StringTokenizer(str, ",");

        System.out.println();

        System.out.println("No. of tokens are:: " + st.countTokens());
        // countTokens() is used to count the no. of tokens.

        System.out.println();

        while (st.hasMoreTokens()) {

            System.out.println(st.nextToken());
        }

        System.out.println();

    }
}