//Taking input through BufferedReader class
//close method

import java.io.*;

class solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("enter the message:: ");
        String str1 = br.readLine();
        System.out.println("Message = " + str1);

        br.close();

        // String str2 = br.readLine();

    }
}

/*
 * NOTE:: After using close method we cannot take any input because using
          close method the connection from keyboard to jvm is become closed.
 */
