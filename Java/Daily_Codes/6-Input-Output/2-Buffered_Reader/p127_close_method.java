//close method

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter first string:: ");
        String str1 = br.readLine();
        System.out.println("String 1 = " + str1);

        System.out.println();

        br.close();

        System.out.print("Enter the character:: ");
        char ch = (char) isr.read();
        System.out.println("Character = " + ch);

        System.out.println();
    }
}
