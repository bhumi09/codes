//Taking input through BufferedReader class
/* 

    Write a program which take input such as society name, 
    wing name and flat no. 
    
*/

import java.io.*;

class solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter society name::  ");
        String name = br.readLine();

        System.out.print("Enter wing:: ");
        char wing = (char)br.read();

        br.skip(1);        
         /*when char is taken before int then char reads the \n at the end of characher, 
         not reads no. Therfore the no is being skip */ 

        System.out.print("Enter Flat no.:: ");
        int flat = Integer.parseInt(br.readLine());

        System.out.println();

        System.out.println("Society Name  = " + name);
        System.out.println("Wing name. = " + wing);
        System.out.println("flat no. = " + flat);

        System.out.println();

    }
}
