//close method

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter first string:: ");
        String str1 = br1.readLine();
        System.out.println("String 1 = " + str1);

        System.out.println();

        br1.close();

        System.out.print("Enter second string:: ");
        String str2 = br2.readLine();
        System.out.println("String 2 = " + str2);

        System.out.println();
    }
}

/*
 * NOTE:: After using close method we cannot take any input because using
          close method the connection from keyboard to jvm is become closed.
 */ 