/*
    Write a program to print User address like society name, wing name and flat no.
    Take all the input on a single line and print the output on different lines.

        Print:: (Enter society name   wing name   flat no)

        Input::  Sun Universe    B   503
        Output::  Society name = Sun Universe
                  Wing Name = B
                  Flat no = 503

 */

 //NOTE:: To split the input we can StringTokenizer class.

 import java.util.*;
 import java.io.*;

 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Enter Society Name   Wing Name   Flat no.");

            String info = br.readLine();

            StringTokenizer obj = new StringTokenizer(info , "   ");

            String token1 = obj.nextToken();
            String token2 = obj.nextToken();
            String token3 = obj.nextToken();

            System.out.println("Society Name =  " + token1);
            System.out.println("Wing Name=  " + token2);
            System.out.println("Flat no. =  " + token3);

            System.out.println();

    }
 }