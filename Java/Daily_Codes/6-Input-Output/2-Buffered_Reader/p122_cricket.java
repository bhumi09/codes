//Taking input through BufferedReader class

import java.io.*;

class solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter Batsman name::  ");
        String name = br.readLine();

        System.out.print("Enter runs:: ");
        int run = Integer.parseInt(br.readLine());

        System.out.println();

        System.out.println("Name  = " + name);
        System.out.println("Run = " + run);

        System.out.println();

    }
}
