//Taking input through BufferedReader class

import java.io.*;

class solution{
    public static void main(String[] args)throws IOException{
        
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        System.out.println();

        System.out.print("Enter your name::  ");
        String name = br.readLine();

        System.out.print("Enter your age:: ");
        int age = Integer.parseInt(br.readLine());

        System.out.println();

       System.out.println("Name  = " + name);
        System.out.println("Age = " + age);

        System.out.println();

    }
}

