/* Try- Catch */

import java.io.*;
class Demo{
    public static void main(String[] args) {
        
        for(int i = 0; i < 10; i++){

            System.out.println("In loop");

            try{

                Thread.sleep(1000);
            }catch(IOException obj){        //error: exception IOException is never thrown in body of corresponding try statement
                
            }
        }
    }
}