/* Real time Example - User Defined Exception */

import java.util.Scanner;;

class TaxException extends RuntimeException{

    TaxException(String msg){

        super(msg);
    }
}
class Tax{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter income");
        int income = sc.nextInt();

        if(income < 10000){

            throw new TaxException("No tax applied");
        }
        if(income > 10000){

            throw new TaxException(" Tax applied");
        }
        System.out.println(income);
    }
}