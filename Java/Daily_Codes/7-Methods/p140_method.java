
//Tryrun codes on methods

class solution{

    void fun(float x){

        System.out.println("In fun method...");
        System.out.println(x);
    }

    public static void main(String[] args) {

        System.out.println();
          
        solution obj = new solution();
        obj.fun(10);
        obj.fun(10.5f);
        obj.fun('A');

        System.out.println();
    }
}

