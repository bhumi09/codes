
//Return value

class solution{

    void fun(int x){

        return x + 10;      //error: incompatible types: unexpected return value
    }

    public static void main(String[] args) {
        
        solution obj = new solution();

        System.out.println();

        System.out.println(obj.fun(10));        //error: 'void' type not allowed here

        System.out.println();
    }
}
