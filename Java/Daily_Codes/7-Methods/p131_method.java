/*
    In Java Methods has two types:: 
        1. Static Method
        2. Non-Static Method (Instance Method)
 */

 class solution{

    public static void main(String[] args) {
       
        fun();
        gun();          //error: non-static method gun() cannot be referenced from a static context

    }

    //static method
    static void fun(){

        System.out.println("\n In fun method...");
    }
    //Non-static method
    void gun(){

        System.out.println("\n In gun method...");
    }

 }

 /*
    NOTE:: Non-static method cannot be called from static context.
            Therefore, this code gives error for non-static method i.e gun();
  */