
//Return value

class solution{

    void fun(int x){

        int y = x + 10;
   
    }

    public static void main(String[] args) {
        
        solution obj = new solution();

        System.out.println();

        int a = obj.fun(10);         //error: incompatible types: void cannot be converted to int
        System.out.println(a);

        System.out.println();
    }
}
