/*
  
1.  Variables and methods which starts from static keyword, 
       those methods and variables are calles as class.

2.  Variables and methods which are not starts from static keyword, 
       those methods and variables are calles as object or instance.

3.  To access not-static methods or variables from static context, we have to make object of class.

 */

 class solution{

    int x = 10;
    static int y = 20;

    public static void main(String[] args) {

        solution obj = new solution();

        System.out.println();
       
        System.out.println(obj.x);
        System.out.println(y);

        obj.fun();
        gun();

    }

    void fun(){

        System.out.println("In fun... \n");
    }

    static void gun(){

        System.out.println("In gun... \n");
    }
 }