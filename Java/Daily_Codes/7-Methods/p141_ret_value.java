
//Return value

class solution{

    int fun(int x){

        return x + 10;
    }

    public static void main(String[] args) {
        
        solution obj = new solution();

        System.out.println();

        //We can call methods from system.out.println(), if the method have return value.
        System.out.println(obj.fun(10));

        System.out.println();
    }
}
