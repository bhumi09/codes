
class solution{

    int x = 10;
    static int y = 20;

    void fun(){

        System.out.println(x);
        System.out.println(y);
    }

    public static void main(String[] args) {
        
        solution obj = new solution();

        obj.fun();

        System.out.println(x);      //error: non-static variable x cannot be referenced from a static context
        System.out.println(y);

    }
}
