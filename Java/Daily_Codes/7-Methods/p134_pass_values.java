// Passing parameter to method

import java.util.*;

class solution {
    
    static void add(int a, int b){

        int ans = a+b;
        System.out.println("\n Addition = " + ans + "\n");
        
    }
        public static void main(String[] args) {
            
            Scanner sc = new Scanner(System.in);

            System.out.println();

            System.out.print("Enter first number::  ");
            int a = sc.nextInt();

            System.out.print("Enter second number::  ");
            int b = sc.nextInt();

            //passing parameter to method
            add(a,b);
            
        }
}