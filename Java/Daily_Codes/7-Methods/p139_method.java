
//Tryrun codes on methods

class solution{

    void fun(int x){

        System.out.println("In fun method...");
        System.out.println(x);
    }

    public static void main(String[] args) {

        System.out.println();
          
        solution obj = new solution();
        obj.fun(10);

        obj.fun(10.5f);     // error: incompatible types: possible lossy conversion from float to int

        System.out.println();
    }
}

