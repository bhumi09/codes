// Passing parameter to method

import java.util.*;

class solution {
    
    //Addition
    static void add(int a, int b){

        int ans = a+b;
        System.out.println("\n Addition of " + a + " and " + b + " = " + ans + "\n");       
    }

    //Subtraction
    static void sub(int a, int b){

        int ans = a-b;
        System.out.println("\n Subtraction of " + a + " and " + b + " = " + ans + "\n");      
    }

    //Multiplication
    static void mul(int a, int b){

        int ans = a*b;
        System.out.println("\n Multiplication of " + a + " and " + b + " = " + ans + "\n");       
    }

    //Division
    static void div(int a, int b){

        int ans = a/b;
        System.out.println("\n Division of " + a + " and " + b + " = " + ans + "\n");
        
    }

    static void mod(int a, int b){

        int ans = a%b;
        System.out.println("\n Modulus of " + a + " and " + b + " = " + ans + "\n");
        
    }
        public static void main(String[] args) {
            
            Scanner sc = new Scanner(System.in);

            System.out.println();

            System.out.print("Enter first number::  ");
            int a = sc.nextInt();

            System.out.print("Enter second number::  ");
            int b = sc.nextInt();

            //passing parameter to method
            add(a,b);
            sub(a,b);
            mul(a,b);
            div(a,b);
            mod(a,b);
            
        }
}