
//Return value

class solution{

    int fun(int x){

        int y = x + 10;
        return y;
    }

    public static void main(String[] args) {
        
        solution obj = new solution();

        System.out.println();

        int a = obj.fun(10);
        System.out.println(a);

        System.out.println();
    }
}
