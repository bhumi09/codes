
class solution{

    void fun(int x){

        System.out.println(x);
    }

    public static void main(String[] args) {

        System.out.println();
        
        System.out.println("In main function...");
        
        solution obj = new solution();
        obj.fun(10);
        System.out.println("End main function...");

        System.out.println();
    }
}