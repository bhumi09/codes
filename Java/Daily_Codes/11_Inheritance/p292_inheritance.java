/* Inheritance - super */

class Parent{

    int x = 10;
    static int y = 20;

    Parent(){

        System.out.println("In parent constructor");
    }
}
class Child extends Parent{

    int x = 100;
    int y = 200;

    Child(){

        System.out.println("In child Constructor");
    }

    void Access(){

        System.out.println("x ::  " + super.x);
        System.out.println("y ::  " + super.y);
        System.out.println("X ::  " + x);
        System.out.println("Y ::  " + y);
    }
}
class Client{
    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.Access();
    }
}