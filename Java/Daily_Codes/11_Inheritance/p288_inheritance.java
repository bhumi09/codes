/* Inheritance */

class Parent{

    int x = 10;

    Parent(){

        System.out.println("Parent Constructor");
    }

    void access(){

        System.out.println("Parent Instance");
    }
}
class Child extends Parent{

    int y = 20;
    Child(){

        System.out.println("Child Constructor");
        System.out.println("X::  " + x);
        System.out.println("Y::  " + y);
    }
}
class Client{
     
    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.access();
    }
}