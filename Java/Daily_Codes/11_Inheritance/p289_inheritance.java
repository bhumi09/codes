/* Static in Parent Class */

class Parent{

    static{

        System.out.println("In parent static class");
    }
}

class Child extends Parent{

    static{

         System.out.println("In child static class");
    }
}

class Client{

    public static void main(String[] args) {
        
        Child obj = new Child();
    }
}