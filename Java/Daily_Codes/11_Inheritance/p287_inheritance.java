/* Inheritance */

class Parent{

    Parent(){

        System.out.println("In parent constructor");
    }

    void parentProperty(){

        System.out.println("Jewellery, Home, Vehicles");
    }
}

class Child extends Parent{

    Child(){

        System.out.println("In child constructor");
    }
}

class Client{

    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.parentProperty();
    }
}