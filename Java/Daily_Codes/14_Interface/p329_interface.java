/* Interface */

interface Demo{

    //static modifier
    static void fun(){

        System.out.println("In fun");
    }

    //default keyword
    default void gun(){

        System.out.println("In gun");
    }
}