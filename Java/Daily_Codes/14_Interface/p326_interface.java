/* Interface */

interface Demo{

    void fun();
    void gun();
}
abstract class DemoChild implements Demo{

    public void fun(){

        System.out.println("In fun");
    }
   
} 
abstract class DemoChild2 extends DemoChild{

    public void gun(){

        System.out.println("In gun");
    }
   
}
class Client{
    public static void main(String[] args) {
        
        Demo obj = new DemoChild2();            //error: DemoChild2 is abstract; cannot be instantiated
        obj.fun();
        obj.gun();
    }
}