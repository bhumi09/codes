/* Bitwise Operator
 
   &  => Bitwise AND
   |  => Bitwise OR
   ^  => Bitwise XOR
   << => Bitwise Left Shift
   >> => Bitwise Right Shift
   >>> => Zero fill Right Shift

 */

class Bitwise{

	public static void main(String args[]){

    System.out.println();
		int x = 8;
		int y = 10;

    System.out.print("Bitwise Left Shift:: ");
		System.out.println(x << 2);

    System.out.print("Bitwise Right Shift:: ");
		System.out.println(y >> 2);

    System.out.println();
	}
}
