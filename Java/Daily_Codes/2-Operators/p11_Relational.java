/* Relational Operator */

class Relational{

	public static void main(String[] args){

		int x = 10;
		int y = 20;
		
		/*
		 *
		if(x < y){

			System.out.println("Hello");
		}
		else{

			System.out.println("Hiiii");
		}
		*/

		if(x){		// error: incompatible types: int cannot be converted to boolean if(x){
			

			System.out.println("Hello");
		}
		else{

			System.out.println("Hiiii");
		}
	}
}
