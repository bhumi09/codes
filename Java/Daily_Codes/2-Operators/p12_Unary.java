/* Unary Operator
 
   ( - , + , ++ , -- )
 */

class Unary{

	public static void main(String[] args){

		int x = 5;
		int y = 7;

		System.out.println("Pre-Increment Operator");
		System.out.println(++x);
		System.out.println(++y);
		
		System.out.println("Pre-Decrement Operator");
		System.out.println(--x);
		System.out.println(--y);
		
		System.out.println();
		System.out.println("x:: " + x);
		System.out.println("y:: " + y);


		System.out.println("Post-Increment Operator");
		System.out.println(x++);
		System.out.println(y++);

		System.out.println("Post-Decrement Operator");
		System.out.println(x--);
		System.out.println(y--);
		
		System.out.println("x:: " + x);
		System.out.println("y:: " + y);
	}
}


