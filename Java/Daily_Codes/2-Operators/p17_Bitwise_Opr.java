/* Bitwise Operator
 
   &  => Bitwise AND
   |  => Bitwise OR
   ^  => Bitwise XOR
   << => Bitwise Left Shift
   >> => Bitwise Right Shift
   >>> => Zero fill Right Shift

 */

class Bitwise{

	public static void main(String args[]){

		int x = 5;
		int y = 7;

		System.out.println(x & y);
		System.out.println(x | y);
	}
}
