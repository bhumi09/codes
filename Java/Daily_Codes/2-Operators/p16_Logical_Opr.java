/* Logical Operator 
	
   	&&  => Logical AND
	||  => Logical OR
	^   => Logical XOR
	~   => Negation

*/

class Logical{

	public static void main(String[] args){

		int x = 5;
		int y = 7;

		//int ans = x < y  && y > x;
		//System.out.println(ans);
		

		//int ans = x && y;
		//System.out.println(ans);
		
		System.out.println(x < y);

		//System.out.println("Logical AND Operator(&&)");
		boolean ans1 = x < y  &&  y < x;

		//System.out.println("Logical OR Operator(||)");
		boolean ans2 = x < y  ||  y < x;


		System.out.println(ans1);
		System.out.println(ans2);

	}
}
