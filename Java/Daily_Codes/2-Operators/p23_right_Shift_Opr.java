/* Right shift operator ( >> )
   Zero fill right shift operator( >>> )

 */

class Bitwise{

	public static void main(String[] args){

		int x = -7;

		System.out.print("Bitwise Right Shift:: ");
		System.out.println(x >> 2);

		System.out.print("Bitwise Right Shift:: ");
		System.out.println(x >>> 2);

		System.out.print("Zero fill Right Shift:: ");
		System.out.println(x >>> 31);
	}
}
