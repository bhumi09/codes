/* Relational Operators
 
   ( < , > , <= , >= , != , == )

*/

class Relational{

	public static void main(String [] args){

		System.out.println();

		int x = 10;
		int y = 20;

		System.out.print("x less than y:: ");
		System.out.println(x < y);
		
		System.out.print("x greater than y:: ");
		System.out.println(x > y);
		
		System.out.print("x less than equal to y:: ");
		System.out.println(x <= y);
		
		System.out.print("x greater than equal to y:: ");
		System.out.println(x >= y);
		
		System.out.print("x equal to y:: ");
		System.out.println(x == y);
		
		System.out.print("x not eual to y:: ");
		System.out.println(x != y);

		System.out.println();
	}
}
