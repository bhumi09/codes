/* Bitwise Operator
 
   &  => Bitwise AND
   |  => Bitwise OR
   ^  => Bitwise XOR
   << => Bitwise Left Shift
   >> => Bitwise Right Shift
   >>> => Zero fill Right Shift

 */

class Bitwise{

	public static void main(String args[]){

		System.out.println();

		int x = 8;
		int y = 10;

		System.out.print(" Bitwise AND:: ");
		System.out.println(x & y);
		
		System.out.print(" Bitwise OR:: ");
		System.out.println(x | y);
		
		System.out.print(" Bitwise XOR:: ");
		System.out.println(x ^ y);

		System.out.println();
	}
}
