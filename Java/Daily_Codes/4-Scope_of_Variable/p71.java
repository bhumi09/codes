
//Scope of Variable
//Try and Error code

class core2web{

	public static void  main(String[] args){

		int x = 10;
		{
			int y = 20;
			System.out.println(x+ "  " + y);
		}
		{

			int y  = 15;					//cannot find symbol
			System.out.println(x+ "  " + y);
		}
		
		System.out.println(x+ "  " + y);
	}
}
		

