//Scope of Variable
//Try and Error code

class core2web{

	public static void  main(String[] args){

		System.out.println();

		int x = 10;
		int y = 20;
		{

			System.out.println(x+ "  " + y);
		}
		{

			x = 15;
			System.out.println(x+ "  " + y);
		}
		
		System.out.println(x+ "  " + y);
		System.out.println();
	}
}
		

