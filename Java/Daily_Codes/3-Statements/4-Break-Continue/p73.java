/*
    Write a program to print prime numbers in range.
 */

class core2web{

	public static void  main(String[] args){

		System.out.println();
		int no = 15;
		int cnt = 0;

		for(int i = 1; i <= no; i++){

			if(no % i == 0){

				cnt++;
			}
			if(cnt > 2){
				break;
			}
		}

		if(cnt == 2){

			System.out.println(no + " is a prime number");
		}
		else{

			System.out.println(no + " is not a prime number");
		}
		System.out.println();
	}
}
