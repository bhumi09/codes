/*
    Write a program to print numbers which are divisible by 3 and 5  or the number divisible by 4 skip it.
 */

class core2web{

	public static void  main(String[] args){

		System.out.println();
		int no = 50;

		for(int i = 1; i <= no; i++){

			if((i % 3 == 0 && i % 5 == 0)  || (i % 4 == 0)){

				continue;
			}
		
			System.out.println(i);
		}
		System.out.println();
	}
}
