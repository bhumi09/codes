/*
    Write a program to print numbers which are divisible by 3 and 5 in range.
 */

class core2web{

	public static void  main(String[] args){

		System.out.println();
		int no = 40;

		for(int i = 1; i <= no; i++){

			if(i % 3 == 0 && i % 5 == 0){

				break;
			}
		
			System.out.println(i);
		}
		System.out.println();
	}
}
