/*
   Take N as input. 
   Print odd integers from 1 to N
    
   input :: 6
   output :: 1    3    5

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 5;
// 1st approach
/*
		for(int i = 1; i <= N; i++){

			if(i % 2 != 0){
				
				System.out.println(i);
			}
		}
		*/

// 2nd approach

		for(int i = 1; i <= N; i = i + 2){

			System.out.println(i);
		}
		System.out.println();
	}
}
