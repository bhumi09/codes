/*
   Take N as input. 
   Count all its factors and print count.
    
   input :: 6
   output :: 4

   input :: 24
   output :: 8

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 24;
		int cnt = 0;

		for(int i = 1; i <= N; i++){

			if(N % i == 0){

				cnt++;
			}
		}
		
		System.out.println("Count of all factors of " + N +  "::  " + cnt + "\n");

	}
}
