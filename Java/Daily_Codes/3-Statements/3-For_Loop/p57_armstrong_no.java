/*
   Take N as input. 
   Print whether N is Armstrong number or not.

   	Prime number :: If any number has only two factors, then these number is called prime number.
    
   input :: 153
   output :: Armstrong number

   input :: 23
   output :: Not a Armstrong number.

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 153;
		int sum = 0;
		int temp = N;

		for(int i = 1; i <= N; i++){

			int rem = temp % 10;
			//N = N / 10;
			sum = sum + rem * rem * rem;
			temp = temp / 10;
		}

		//sum = sum + rem * rem * rem;
		
		if(sum == N){
			
			System.out.println(N + " is a armstrong number ");
		}
		else{

			System.out.println( N + " is not a armstrong number ");
		}
		System.out.println();

	}
}
