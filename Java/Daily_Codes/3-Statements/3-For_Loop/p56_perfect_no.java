/*
   Take N as input. 
   Print whether N is perfect or not.

   	Perfect number :: Perfect number is equal to the sum of its factors without including itself.
    
   input :: 6
   output :: perfect number

   input :: 4
   output :: Not a perfect number.

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 6;
		int sum = 0;

//1st Approach

		for(int i = 1; i < N; i++){

			if(N % i == 0){

				sum = sum + i;
			}
		}


//2nd Approach
/*
		for(int i = 1; i <= N; i++){

			if(N % i == 0){

				sum = sum + i;
			}

		}
		sum = sum - N;
*/

		if(sum == N){
			
			System.out.println(N + " is  a perfect number ");
		}
		else{

			System.out.println(N + " is not a perfect number ");
		}
		System.out.println();
	}
}
