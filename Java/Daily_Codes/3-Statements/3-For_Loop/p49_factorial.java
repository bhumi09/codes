/*
   Take N as input. 
   Print its factorial.

   input :: 5
   output :: 120

   input :: 4
   output :: 24

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 5;
		int fact = 1;

		for(int i = 1; i <= N; i++){
	
			fact = fact * i;
		}

		System.out.println("Factorial of " + N + "is  " + fact + "\n");
	}
}
