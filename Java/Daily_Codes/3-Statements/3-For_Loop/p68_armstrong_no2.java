/* Armstrong Number for N digits . */

class solution{

	public static void main(String[] args){

		System.out.println();

		int no = 1634;
		int cnt = 0;
		int temp1 = no;
		int temp2 = no;
		int sum = 0;

		while(temp1 != 0){

			cnt++;
			temp1 = temp1 / 10;
		}
		while(temp2 != 0){

			int rem = temp2 % 10;
			int mul = 1;

			for(int i = 1; i <= cnt; i++){

				mul = mul * rem;
			}

			sum = sum + mul;
			temp2 = temp2 / 10;
		}

		if(sum == no){

			System.out.println(no + " is a armstrong number");
		}
		else{

			System.out.println(no + " is not a armstrong number");
		}
		System.out.println();
	}
}

