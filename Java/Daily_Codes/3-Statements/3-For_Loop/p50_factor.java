/*
   Take N as input. 
   Print all its factors.
    
   input :: 6
   output :: 1    2    3    6

   input :: 24
   output :: 1    2    3    4    6    8    12    24

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 24;

		for(int i = 1; i <= N; i++){

			if(N % i == 0){
				
				System.out.println(i);
			}
		}
		System.out.println();

	}
}
