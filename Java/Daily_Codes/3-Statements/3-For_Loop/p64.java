
//Try and Error code

class core2web{

	public static void main(String[] args){

		System.out.println("Before for loop");

		for(int i, j; i < 3; i++, j++){		//error: variable i, j might not have been initialized

			System.out.println("inside for");
		}
		System.out.println("After for loop");

	}
}

