/*
   Take N as input. 
   Print integers from 1 to N
    
   input :: 5
   output :: 1    2    3    4    5

*/

class solution{
	
	public static void main(String[] args){

		int N = 5;

		System.out.println();
		for(int i = 1; i <= N; i++){

			System.out.println(i);
		}

		System.out.println();
	}
}
