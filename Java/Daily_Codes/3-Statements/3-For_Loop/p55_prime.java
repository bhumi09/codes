/*
   Take N as input. 
   Print whether it is prime or not.

   	Prime number :: If any number has only two factors, then these number is called prime number.
    
   input :: 5
   output :: prime number

   input :: 8
   output :: Not a prime number.

*/

class solution{
	
	public static void main(String[] args){

		System.out.println();
		int N = 5;
		int cnt = 0;

		for(int i = 1; i <= N; i++){

			if(N % i == 0){

				cnt++;
			}
		}

		if(cnt == 2){
			
			System.out.println(N + " is  a prime number ");
		}
		else{

			System.out.println(N + " is not a prime number ");
		}

		System.out.println();

	}
}
