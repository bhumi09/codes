
//Try and Error code

class core2web{

	public static void main(String[] args){

		System.out.println("Before for loop");

		for(int i , j; i < 3; i++){			//error: variable i might not have been initialized

			System.out.println("inside for");
		}

		System.out.println("after for loop");
	}
}

