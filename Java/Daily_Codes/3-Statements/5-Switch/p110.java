// Switch statement( using break )

/* 
   NOTE ::1.  Duplicate cases are not allowed in switch case.
          2.  Arithmatic operations are allowed in switch case.
 */

class solution {

    public static void main(String[] args) {

        int N = 5;

        switch (N) {

            case 1:
                System.out.println("1");
                break;

            case 1+1:
                System.out.println("2");
                break;

            case 1+2:
                System.out.println("first-5");
                break;

            case 5:
                System.out.println("second-5");
                break;

            case 2:                                 //error: duplicate case label
                System.out.println("second-2");
                break;

            default:
                System.out.println("No matches found");
                break;

        }

        System.out.println("After switch");
        System.out.println();
    }

}