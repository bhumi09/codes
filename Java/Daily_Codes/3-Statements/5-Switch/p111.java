// Switch statement( using break )
//Try and Error code

/* 
   NOTE ::1.  Duplicate cases are not allowed in switch case.
          2.  As a case value charachters are allowed in switch case.
 */

 class solution {

    public static void main(String[] args) {

        int N = 5;

        switch (N) {

            case 'A':                    
                System.out.println("1");
                break;

            case 65:                    //error: duplicate case label
                System.out.println("2");
                break;

            case 'B':
                System.out.println("first-5");
                break;

            case 66:                       //error: duplicate case label
                System.out.println("second-5");
                break;

            default:
                System.out.println("No matches found");
                break;

        }

        System.out.println("After switch");
        System.out.println();
    }

}