// Nested-Switch statement( using break )
//Hotel Menu Example

/* 
   NOTE :: 1. As a case value strings are allowed in switch case.
 */

 class solution {

    public static void main(String[] args) {

       String str ="Non-Veg";
      
       System.out.println();
       System.out.println("___OH PUNE!!___");

        switch (str) {

            case "Veg":{

                String str1 = "Starter";

                switch(str1){

                    case "Starter":
                                    System.out.println("masala papad");
                                    break;

                    case "main-course":
                                    System.out.println("paneer");
                                    break;

                    default:
                            System.out.println("Dish not available");
                }
            }
            break;

            case "Non-Veg":{

                String str2 = "Main-course";

                switch(str2){

                    case "starter":
                                    System.out.println("Boiled-Eggs");
                                    break;

                    case "Main-course":
                                    System.out.println("Shahi Dum Biryani");
                                    break;

                    default:
                            System.out.println("Dish not available");
                }

            }
            break;
        }

        System.out.println();
    }

}