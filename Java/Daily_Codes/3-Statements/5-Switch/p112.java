// Switch statement( using break )
//Try and Error code

/* 
   NOTE ::1. Duplicate cases are not allowed in switch case.
          2. Constant expressions are required in switch case.
 */

 class solution {

    public static void main(String[] args) {

        int N = 5;

        switch (N) {

            case a:                 // error: cannot find symbol
                System.out.println("1");
                break;

            case b:
                System.out.println("2");
                break;

            case a+b:
                System.out.println("3");
                break;

            case a+a+b:
                System.out.println("4");
                break;

            case b+b+a:
                System.out.println("5");
                break;

            default:
                System.out.println("No matches found");
                break;

        }

        System.out.println("After switch");
        System.out.println();
    }

}
