// Switch statement

class solution {

    public static void main(String[] args) {

        System.out.println();
        int N = 6;

        switch (N) {

            case 1:
                System.out.println("One");

            case 2:
                System.out.println("Two");

            case 3:
                System.out.println("Three");

            case 4:
                System.out.println("Four");

            case 5:
                System.out.println("Five");

            default:
                System.out.println("No matches found");

        }

        System.out.println("After switch");
        System.out.println();
    }

}