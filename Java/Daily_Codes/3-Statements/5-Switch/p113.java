// Switch statement( using break )
//Try and Error code

/* 
   NOTE ::1.  Duplicate cases are not allowed in switch case.
          2.  As a case value strings are allowed in switch case.
 */

 class solution {

    public static void main(String[] args) {

       String str ="Mon";

        switch (str) {

            case "Mon":
                System.out.println("Monday");
                break;

            case "Tue":
                System.out.println("Tuesday");
                break;

            default:
                System.out.println("No matches found");
                break;

        }

        System.out.println("After switch");
        System.out.println();
    }

}