/* 
   Take two integers A and B as input 
   Print max of two.
   If A and B are equal then print "Both are Equal".

   	input 1: A = 5
	         B = 7
	output:  7 is greater.

	input 2: A = 5
		 B = 5
	output:  Both are equal

*/

class Max{

	public static void main(String[] args){

		System.out.println();
		int A = 5;
		int B = 5;

		if(A == B){

			System.out.println("Both are equal");
		}
		else if(A > B){

			System.out.println(A + "is greater");
		}
		else{
			System.out.println(B + "is greater");
		}

		System.out.println();
	}
}



