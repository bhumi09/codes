/* 
  Given an integer value as input.
	  Print Fizz if it is divisible by 3.
	  Print Buzz if it is divisible by 5.
	  Print Fizz-Buzz if it is divisible by both.
	  If not then print "Not Divisible by both".

*/

class IsDivisible{

	public static void main(String[] args){

		System.out.println();
		int no = 15;

		if(no % 3 == 0 && no % 5 == 0){

			System.out.println("Fizz-Buzz");
		}
		else if(no % 3 == 0){

			System.out.println( "Fizz");
		}
	
		else if(no % 5 == 0){

			System.out.println( "Buzz");
		}
		else{

			System.out.println("not divisible by both");
		}
		System.out.println();
	}
}
