/* 
   Take two integers A and B as input 
   Print max of two.
   Assume A nad B are not equal.

   	input: A = 5
	       B = 7
	output: 7 is greater.

*/

class Max{

	public static void main(String[] args){

		System.out.println(); 

		int A = 5;
		int B = 7;

		if(A > B){

			System.out.println(A + " is greater");
		}
		else{
			System.out.println(B + " is greater");
		}

		System.out.println();
	}
}



