/* 
  Take ans integer as input and print whether it is divisible by 4 or not.

  	input  :: 5
	output :: Not divisible

*/

class IsDivisible{

	public static void main(String[] args){

		System.out.println();
		int no = 5;

		if(no % 4 == 0){

			System.out.println( no + " is divisible by 4");
		}
		else{

			System.out.println( no + " is not divisible by 4");
		}
		System.out.println();
	}
}
