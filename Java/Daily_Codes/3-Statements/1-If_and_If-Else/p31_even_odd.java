/* 
  Take ans integer as input.
  Check the given input is even or odd.

  	input  :: 5
	output :: Odd

*/

class IsEvenOdd{

	public static void main(String[] args){

		System.out.println();
		int no = 5;

		if(no % 2 == 0){

			System.out.println( no + " is Even");
		}
		else{

			System.out.println( no + " is Odd");
		}
		System.out.println();
	}
}
