/* 
 Given an integer age as input.
 Print "Eligible to vote" if the person is eligible to vote 

 */

class Vote{

	public static void main(String[] args){

		int age = 20;

		if(age > 18){

			System.out.println("Eligible to vote...");
		}
	}
}
