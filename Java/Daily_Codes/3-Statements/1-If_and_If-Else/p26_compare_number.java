/* 
  
   Given an integer as input.
   Check given input is greater than 5 or not 
 
 */

class Demo{

	public static void main(String[] args){

		System.out.println();
		int no = 10;

		if(no > 5){

			System.out.println(no+ " is greater than 5");
		}

		System.out.println(no+ " is smaller than 5");

		System.out.println();
	}
}
