/* 
   Given the temprature of a person is feraheit.
   Print whether person has high, normal and kow temperature.
   	
   	>98.6 		      :: High
	98.0 <= and <= 98.6   :: Normal
	< 98.0 a              :: Low
*/

class Temp{

	public static void main(String[] args){

		System.out.println();
		float temp = 98.0f;

		if(temp > 98.6f){

			System.out.println("High");
		}
		else if(temp < 98.0f){

			System.out.println("Low");
		}
		else{
			System.out.println("Medium");
		}
		System.out.println();
	}
}



