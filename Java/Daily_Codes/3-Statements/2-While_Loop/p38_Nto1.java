/*
   Print integers from 8 to 1 using while loop.
   Output::  8  7  6  5  4  3  2  1

  */

class Demo{

	public static void main(String[] args){

		System.out.println();
		int no = 8;

		while(no >= 1){

			System.out.print(no + "  ");
			no--;
		}
		System.out.println();
	}
}
