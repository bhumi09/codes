/* 
  Given  an integer N as input.
  Reverse it. 

  	Input:  6531
	Output: 1   3   5    6  

*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 6531;
		int rev = 0;

		while(no != 0){

			int rem = no % 10;
			rev = rev * 10 + rem;
			no = no / 10;
			//System.out.print(rev);
		}

		System.out.print("Reverse no. is " + rev);
		System.out.println();
	}
}
