/* 
  Take an integer N as input.
  print numbers which are divisible by 3

  	Input:  20
	Output: 3  6  9  12   15  18

*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 20;
		int i = 1;

		while(i <= no){

			if(i % 3 == 0){

				System.out.print(i + "  ");
			}
			i++;
		}
		System.out.println();
	}
}
