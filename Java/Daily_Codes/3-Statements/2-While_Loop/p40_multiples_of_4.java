/*
  Print multiples of 4 till N.
  	input:  22
	output: 4  8  12  16  20
*/

class multiple{

	public static void main(String[] args){

		System.out.println();
		int i = 4;
		int N = 22;

		while(i <= N){

			if(i % 4 == 0){

				System.out.print(i + "  ");
			}
			i = i+ 4;
		}

		System.out.println();
	}
}
