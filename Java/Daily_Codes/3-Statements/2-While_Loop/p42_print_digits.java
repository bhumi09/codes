/* 
  Given  an integer N as input.
  print all its digits 

  	Input:  6531
	Output: 1   3    5    6

*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 6531;

		while(no != 0){

			int rem = no % 10;
			//System.out.print(" "+  rem +  " ");
			no = no / 10;
		
			System.out.print(" "+  rem +  " ");
		}

		System.out.println();

	}
}
