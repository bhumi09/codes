/* 
  Take an integer N as input.
  print odd integers fro 1 to N using loop.

  	Input:  10
	Output: 1  3  5  7  9

*/

class odd{

	public static void main(String[] args){

		System.out.println();
		int no = 10;
		int i = 1;

		while(i <= no){

			if(i % 2 != 0){

				System.out.print(i + "  ");
			}
			i++;
		}
		System.out.println();
	}
}
