/* 
  Given  an integer N as input.
  print sum its digits 

  	Input:  6531
	Output: 15  

*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 6531;
		int sum = 0;

		while(no != 0){

			int rem = no % 10;

			no = no / 10;
			sum = sum + rem;
		}

		System.out.println("sum of all digits =" + sum + "\n");

	}
}
