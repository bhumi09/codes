/* 
  Take an integer N as input.
  print perfect square till N.

 	Perfect Square:: An integer whose square roor is an integer. 

  	25 -> 5		yes
	81 -> 9		yes
	1 ->  1		yes
	10 -> 3.13	No

	Input :: 30
	Output :: 1    4    9    16    25

*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 30;
		int i = 1;

		while(i * i <= no){
			
			System.out.println(i * i);
			i++;
			
		}

		System.out.println();
	}
}
