/* 
  Print integers from 1 to 10 using while loop.

	Output:1  2  3  4  5  6  7  8  9  10
*/

class Demo{

	public static void main(String[] args){

		System.out.println();
		int no = 1;

		while(no <= 10){

			System.out.print(no + "   ");
			no++;
		}

		System.out.println();
	}
}
