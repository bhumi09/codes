/* Access Specifier in Method Overriding */

class Parent{

    void fun(){

        System.out.println("Parent fun");
    }
}
class Child extends Parent{
    private void fun(){                       //error: attempting to assign weaker access privileges; was package

        System.out.println("Child fun");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj  = new Child();
        obj.fun();                                 
    }
}