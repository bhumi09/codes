/* Static modifier in Overriding - Scenario 1*/

class Parent{
    static void fun(){

        System.out.println("Parent fun");
    }
}
class Child extends Parent{
    void fun(){                                 //error: fun() in Child cannot override fun() in Parent 
                                                //overridden method is static

        System.out.println("Child fun");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Parent();
        obj.fun();
    }
}