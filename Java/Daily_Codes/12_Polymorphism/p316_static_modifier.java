/* Static modifier in Overriding - Scenario 3 */

class Parent{
    void fun(){

        System.out.println("Parent fun");
    }
}
class Child extends Parent{
    static void fun(){                      //error: fun() in Child cannot override fun() in Parent
                                                // overriding method is static                    

        System.out.println("Child fun");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Child();
        obj.fun();
    }
}