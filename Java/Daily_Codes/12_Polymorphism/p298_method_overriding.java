/* Method Overriding - Scenario 1 */

class Parent{

    Parent(){

        System.out.println("Paret Constructor");
    }
    void fun(){

        System.out.println("Parent fun");
    }
}
class Child extends Parent{

    Child(){

        System.out.println("Child Constructor");
    }

    void fun(){

        System.out.println("Child fun");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Child();
        obj.fun();
    }
}