/* Final modifier in overriding */

class Parent{
    final void fun(){
        
        System.out.println("Parent fun");
    }
}
class Child extends Parent{
    void fun(){                                      //error: overridden method is final

        System.out.println("Child fun");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Child();
        obj.fun();
    }
}