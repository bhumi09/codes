/* Method Overriding */

class Parent{

    Parent(){

        System.out.println("In parent Constructor");
    }

    void property(){

        System.out.println("Home, Jewellery, cars");
    }

    void marry(){

        System.out.println("XYZ");
    }
}

class Child extends Parent{

    Child(){

        System.out.println("In child Constructor");
    }

    void marry(){

        System.out.println("ABC");       
    }
}
class Client{
    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.property();
        obj.marry();
    }
}