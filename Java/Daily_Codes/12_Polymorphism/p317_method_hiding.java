/* Method Hiding */

class Parent{

    static  void fun(){

        System.out.println("Parent fun");
    }
}
class Child extends Parent{

    static void fun(){

        System.out.println("Child fun");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Parent();
        obj.fun();

        Child obj1 = new Child();
        obj1.fun();

        Parent obj3 = new Child();
        obj3.fun();
    }
}