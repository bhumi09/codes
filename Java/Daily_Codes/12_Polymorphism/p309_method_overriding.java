/*
     Method Overriding - Scenario 
     Covariant Return Type
*/

class Parent {

    Object fun() {

        Object obj = new Object();
        System.out.println("Parent fun");
        return obj; // return new Object()
    }
}

class Child extends Parent {

    String fun() {

        System.out.println("Child fun");
        return "Bhumika";
    }
}

class Client {
    public static void main(String[] args) {

        Parent p = new Child();
        p.fun();
    }
}