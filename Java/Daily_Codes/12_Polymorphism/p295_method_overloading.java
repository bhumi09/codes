/* Method Overloading */

class Demo{

    void fun(int x){                                //Method Signature - fun(int)

        System.out.println("x :  "+ x);
    }

    void fun(float y){                              //Method Signature - fun(float)

        System.out.println("y :  " + y);
    }

    void fun(Demo obj){                             //Method Signature - fun(Demo)

        System.out.println("In Demo Parameter");
        System.out.println(obj);
    }
    public static void main(String[] args) {
        
        Demo obj = new Demo();
        obj.fun(10);
        obj.fun(10.5f);
        
        Demo obj1 = new Demo();
        obj1.fun(obj);
    }

    
}