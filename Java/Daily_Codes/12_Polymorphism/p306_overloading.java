/*  Method Overloading - Scenario  */

class Demo{

    void fun(Object obj){

        System.out.println("Object");
    }
}
class Client{
    public static void main(String[] args) {
        
        Demo obj = new Demo();

        obj.fun("BHUMIKA");
        obj.fun(new StringBuffer("bhumika"));
        obj.fun(null);
    }
}