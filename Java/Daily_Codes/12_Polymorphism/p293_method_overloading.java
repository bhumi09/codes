/* 
    Method Overloading 
    Try and error code
*/

class Demo{

    void fun(int x){                        // Method Signature - fun(int)

        System.out.println("x :  " + x);
    }

    void fun(int x){                            // Method Signature - fun(int)
                                                //error: method fun(int) is already defined in class Demo

        System.out.println("x :  " + x);
    }
}