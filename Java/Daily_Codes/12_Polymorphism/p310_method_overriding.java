/* Access Specifier in Method Overriding */

class Parent{
    public void fun(){

        System.out.println("Parent fun");
    }
}
class Child extends Parent{
    void fun(){                                 //Error: fun() in Child cannot override fun() in Parent

        System.out.println("Child fun");
    }
}