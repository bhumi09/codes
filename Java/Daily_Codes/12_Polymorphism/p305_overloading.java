/*  Method Overloading - Scenario */

class Demo{

    void fun(String str){

        System.out.println("String");
    }
    void fun(StringBuffer str1){

        System.out.println("StringBuffer");
    }
}

class Client{
    public static void main(String[] args) {
        
        Demo obj = new Demo();
        obj.fun("BHUMIKA");
        obj.fun(new StringBuffer("bhumika"));
       // obj.fun(null);                          //error: reference to fun is ambiguous
    }
}