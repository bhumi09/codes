/* 
    Method Overloading 
    Try and error code
*/

class Demo{

    int fun(int x){

        System.out.println("x :  " + x);
    }

    float fun(int x){                       //error: method fun(int) is already defined in class Demo

        System.out.println("x :  " + x);
    }
}