//Constructor- Real time Example

import javax.sound.sampled.SourceDataLine;

class Pub{

    int noOfPeople = 5;
    static float budget = 10000f;

    Pub(){

        System.out.println("...Cafe CO2...");
    }

    void entry(){

        System.out.println("Number of People::  " + noOfPeople);
    }

    static void budget(){

        System.out.println("Budget::  " + budget);
    }
    public static void main(String[] args) {
       
        System.out.println();
        Pub obj = new Pub();
        
        String address = "Bhugaon, Pune";
        System.out.println("Address::   " + address);

        obj.entry();
        budget();
        System.out.println();
    }
}