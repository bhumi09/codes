//Access Specifier
//Private

class Core2web{

    int noOfCourses = 10;
    private String fav_Course = "Java Programming";

    void display(){

        System.out.println();

        System.out.println("No. of courses:: " + noOfCourses);
        System.out.println("Favourite course:: " + fav_Course);

        System.out.println();
    }
}

class Student{
    public static void main(String[] args) {
        
        Core2web obj = new Core2web();
        obj.display();

        System.out.println("No. of courses:: " + obj.noOfCourses);
        //System.out.println("Favourite course:: " + obj.fav_Course);       // error: fav_Course has private access in Core2web
    }
}