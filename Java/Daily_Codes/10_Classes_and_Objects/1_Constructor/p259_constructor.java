//Try and Error code
//Access Specifier

class Employee{

    int empId = 10;
    String empName = "Kanha";

    void empInfo(){

        System.out.println("ID:: " + empId);
        System.out.println("Name:: " + empName);
    }
}

class mainDemo{
    public static void main(String[] args) {
        
        Employee emp = new Employee();
        emp.empInfo();

        System.out.println("ID:: " + emp.empId);
        System.out.println("Name:: " + emp.empName);


    }
}