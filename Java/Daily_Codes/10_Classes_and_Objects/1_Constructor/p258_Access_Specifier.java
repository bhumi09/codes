//Access Specifier
//Try and error code

class Demo{

    int x = 10;
    private int y = 20;

    void fun(){

        System.out.println("x:: " + x);
        System.out.println("y:: " + y);
    }
}

class mainDemo{
    public static void main(String[] args) {
        
        Demo obj = new Demo();
        obj.fun();

        System.out.println("x:: " + obj.x);
        System.out.println("y:: " + obj.y);         //error: y has private access in Demo

        System.out.println("x:: " + x);             // error: cannot find symbol
        System.out.println("y:: " + y);             // error: cannot find symbol
    }
}