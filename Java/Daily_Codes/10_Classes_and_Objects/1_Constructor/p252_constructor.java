//Constructor

class Demo{

    int x = 10;
    String str = "Bhumika";

    void fun(){

        String str2 = "Bhumika";                //According to JAVA 1.5, str2 goes in StringConstantPool on the method area
        String str3 = new String("Shejwal");        //According to JAVA 1.5, str3 goes on Heap section.
    }
    public static void main(String[] args) {
        
        Demo obj = new Demo();
        obj.fun();
    }
}