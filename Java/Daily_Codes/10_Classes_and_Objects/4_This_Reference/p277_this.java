/* Hidden This Reference */

class Demo{

    int x = 10;

    Demo(){                                             //Demo(Demo this)

        System.out.println("In Constructor");
        System.out.println(this);
        System.out.println("this.x ::  " + this.x);
    }
    void fun(){                                         //fun(Demo this)

        System.out.println(this);
        System.out.println("x ::  " + x);
    }
    public static void main(String[] args) {
        
        Demo obj = new Demo();                          //Demo(obj)         
        System.out.println(obj);
        obj.fun();                                      //fun(obj)
        
    }
}