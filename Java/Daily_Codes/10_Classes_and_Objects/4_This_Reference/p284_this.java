/* Calling constructor fromm another constructor */

class Demo{

    int x = 10;

    Demo(){

        System.out.println("No argument constructor");
    }

    Demo(int x){

        this();
        System.out.println("Parametarized Constructor");
    }
    public static void main(String[] args) {
        
        Demo obj = new Demo(50);
    }
}