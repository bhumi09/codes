/* Method Signature */

class Demo{

    int x = 10;

    Demo(){

        System.out.println("In constructor");
    }

    Demo(){                     //error: constructor Demo() is already defined in class Demo

        System.out.println("In constructor 2");
    }
}