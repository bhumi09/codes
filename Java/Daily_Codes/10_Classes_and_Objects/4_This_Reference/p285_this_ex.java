/* 
    This Reference
    Real time Example::Cricket Player 
*/

class Player{

    private int jerNo = 18;
    private String PName = null;

    Player(int jerNo, String PName){

        this.jerNo = jerNo;
        this.PName = PName;
        System.out.println("In Constructor");
    }

    void PlayerInfo(){

        System.out.println("Jersy No::  " + jerNo);
        System.out.println("Player Name::  " + PName);
    }
}
class Client{

    public static void main(String[] args) {

        Player obj1 = new Player(18, "Virat Kohli");
        obj1.PlayerInfo();

        Player obj2 = new Player(7, "MS. Dhoni");
        obj2.PlayerInfo();

        Player obj3 = new Player(45,"Rohit Sharma");
        obj3.PlayerInfo();
    }
}










