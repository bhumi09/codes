/* Calling constructor fromm another constructor */

class Demo{

    int x = 10;

     Demo(){

        this(70);                                      //  error: recursive constructor invocation
        System.out.println("In no argument constructor");
     }

     Demo(int x){

        this();
        System.out.println();
     }

     public static void main(String[] args) {
        
        Demo obj = new Demo(50);
     }
}