/* Hidden This Reference */

class Demo{

    int x = 10;                                      //this.x

    Demo(int x){                                    //Demo(Demo this, int x)

        System.out.println("x::  " + x);
    }
    public static void main(String[] args) {
        
        Demo obj = new Demo(10);
    }
}