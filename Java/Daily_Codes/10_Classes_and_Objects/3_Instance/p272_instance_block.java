/* Instance Block */

class InstanceDemo {

    int x = 10;

    InstanceDemo() {

        System.out.println("In constructor");
    }

    // Instance Block
    {
        System.out.println("Instance Block 1");
    }

    public static void main(String[] args) {

        InstanceDemo ID = new InstanceDemo();
        System.out.println("In main method");
    }

    {
        System.out.println("Instance Block 2");
    }
}