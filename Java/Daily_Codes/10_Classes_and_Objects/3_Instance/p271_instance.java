/* Instance variable and Instance method */

class InstanceDemo{

    int x = 10;
    static int y = 20;

    void fun(){

        System.out.println("x ::  " + x);
        System.out.println("y ::  " + y);
    }
    void gun(){

        fun();
    }
    public static void main(String[] args) {

        InstanceDemo ID = new InstanceDemo();
        ID.gun();        
    }
}