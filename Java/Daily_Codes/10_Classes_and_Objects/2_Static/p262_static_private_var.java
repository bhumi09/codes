/* Static and Private variable */

class Demo{

    int x = 10;
    private int y = 20;
    static int  z = 30;

    void display(){

        System.out.println();

        System.out.println("x ::  " + x);
        System.out.println("y ::  " + y);
        System.out.println("z ::  " + z + "\n");
    }
}

class client{

    public static void main(String[] args) {
        
        Demo obj1 = new Demo();
        Demo obj2 = new Demo();

        obj1.display();

        obj1.x = 100;
       // obj1.y = 200;           //error: y has private access in Demo
        obj1.z = 300;

        obj1.display();
        obj2.display();
    }
}