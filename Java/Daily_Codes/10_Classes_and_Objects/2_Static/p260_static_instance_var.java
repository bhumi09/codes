//Static and instance variable

class Employee{

    int empId = 10;
    String empName = "Kanha";
    static int y = 50;

    void empInfo(){

        System.out.println();

        System.out.println("ID:: " + empId);
        System.out.println("Name:: " + empName);
        System.out.println("y:: " + y);

        System.out.println();
    }
}

class mainDemo{
    public static void main(String[] args) {
        
        Employee emp1 = new Employee();
        Employee emp2 = new Employee();

        emp1.empInfo();
        emp2.empInfo();

        System.out.println("--------------------------------------------------------");

        emp2.empId = 20;
        emp2.empName ="Rahul";
        emp2.y = 500;

        emp1.empInfo();
        emp2.empInfo();
    }
}