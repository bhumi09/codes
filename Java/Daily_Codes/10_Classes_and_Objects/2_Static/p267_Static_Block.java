/* Static Block */

class StaticDemo{

    int x = 10;
    static int y = 20;

    static{
        
        System.out.println("\n" + "In Static Block 1");
    }
    public static void main(String[] args) {
        
        System.out.println("In main method");

        StaticDemo SD = new StaticDemo();
        System.out.println("x ::  " + SD.x);
    }

    static{
        
        System.out.println("In Static Block 2");
        System.out.println("y ::  " + y);
    }

}