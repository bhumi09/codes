/* system.exit() and Static Block */

class StaticDemo{
    
    static{

        System.out.println("Hello World..");
        System.exit(0);
    }
    public static void main(String[] args) {
        
        System.out.println("Main method");
    }
}