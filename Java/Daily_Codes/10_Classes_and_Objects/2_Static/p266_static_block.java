/* Static Block */

class StaticDemo{

    static int x = 10;
    
    static{

        System.out.println("\n" + "In static block");
        System.out.println("x ::  " + x );
    }
    public static void main(String[] args) {
        
        System.out.println("In main method" + "\n");
    }
}
