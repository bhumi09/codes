/* 
    Static Variable and Static Block
    Try run code 
*/

class StaticDemo{

    static int x = 10;
    static{

        // static int y = 20;              //error: illegal start of expression
    }

    void fun(){

       // static int z = 30;               // error: illegal start of expression
    }

    static void gun(){

        //static int z = 40;               // error: illegal start of expression
    }
}