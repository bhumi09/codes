//Static and instance variable

class WebSeries{

    int totalEpisode = 16;
    String sName = "Asur";
    static int totalSeason = 2;

    void seriesInfo(){

        System.out.println();

        System.out.println(" Series Name:: " + sName);
        System.out.println("Total Seasons:: " + totalSeason);
        System.out.println("Total Episodes:: " + totalEpisode);

        System.out.println();
    }
}

class mainDemo{
    public static void main(String[] args) {
        
        WebSeries WS1 = new WebSeries();
        WebSeries WS2 = new WebSeries();

        WS1.seriesInfo();
        WS2.seriesInfo();

        System.out.println("--------------------------------------------------------");

        WS2.totalEpisode = 40;
        WS2.sName ="Flames";
        WS2.totalSeason = 5;

        WS1.seriesInfo();
        WS2.seriesInfo();
    }
}