/* Static Variable and Static Method */

class StaticDemo{

    int x = 10;
    static int y = 20;

    void fun1(){
        
        System.out.println("x ::  " + x);
        System.out.println("y ::  " + y);
    }

    static void fun2(){

        //System.out.println("x ::  " + x);     //error: non-static variable x cannot be referenced from a static context
        System.out.println("y ::  " + y);
    }
}
class client{
    public static void main(String[] args) {
        
        StaticDemo SD = new StaticDemo();

        SD.fun1();
        SD.fun2();
        
        System.out.println("x ::  " + SD.x);
        System.out.println("y ::  " + SD.y);
    }
}