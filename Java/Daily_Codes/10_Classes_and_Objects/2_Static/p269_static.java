/* Static Block */

class StaticDemo{

    static{
        
        System.out.println("In Static Block 1");
    }
    public static void main(String[] args) {
        
        System.out.println("In StaticDemo main");
    }
}

class Client{

    static{
        
        System.out.println("In Static Block 2");
    }
    public static void main(String[] args) {
        
        System.out.println("In Client main");
        StaticDemo obj = new StaticDemo();
    }
    
    static{
        
        System.out.println("In Static Block 3");
    }
    
}