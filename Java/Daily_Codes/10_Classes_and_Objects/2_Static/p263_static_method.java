/* Static Variable and Static Method */

class StaticDemo{

    static int x = 10;
    static int y = 20;

    static void disp(){

        System.out.println("x ::  " + x);
        System.out.println("y ::  " + y);
    }
}
class client{
    public static void main(String[] args) {

        System.out.println("x ::  " + StaticDemo.x);
        System.out.println("y ::  " + StaticDemo.y);
        
        StaticDemo.disp();
    }
}