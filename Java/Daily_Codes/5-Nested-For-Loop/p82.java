
/*
     7    7    7
     7    7    7
     7    7    7  
     7    7    7

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;

        for (int i = 1; i <= N+1; i++) {
            
            for (int j = 1; j <= N; j++) {
            
                System.out.print("\t" + " 7 ");
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

