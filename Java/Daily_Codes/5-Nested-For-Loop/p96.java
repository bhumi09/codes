

/*

     1
     2    3
     4    5    6
     7    8    9    10
     11   12   13   14    15     

*/


import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.println("Enter the no. of rows:: ");
        int N = obj.nextInt();
    
        int no = 1;

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= i; j++) {

                    System.out.print("\t" + no++);
                                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

