

/*

     1
     1    2
     1    2    3
     1    2    3     4    

*/


import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();
    
        for (int i = 1; i <= N; i++) {

            int no = 1;
            
            for (int j = 1; j <= i; j++) {

                    System.out.print("\t" + no++);
                                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

