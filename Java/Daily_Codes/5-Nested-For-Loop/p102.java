

/*

    1
    4     9
    16    25    36
    49    64    81    100  

*/

import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();

        int no = 1;

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= i; j++) {

                    System.out.print("\t" + no * no);
                    no++;
                                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}




