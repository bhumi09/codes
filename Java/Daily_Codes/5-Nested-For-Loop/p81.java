
/*
     A    A    A
     A    A    A
     A    A    A  

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= N; j++) {
            
                System.out.print("\t" + " A ");
            }
            
            System.out.println();
        }

        System.out.println();
    }
}
