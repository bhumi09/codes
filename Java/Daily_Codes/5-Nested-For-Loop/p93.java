
/*
     *    -    -    *
     *    -    -    *
     *    -    -    *
     *    -    -    *
       

*/

//First Approach

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 4;

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= N; j++) {

                if(j == 1 || j == N){

                    System.out.print("\t" + "*");
                }
                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

