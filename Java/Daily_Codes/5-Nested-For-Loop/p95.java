

/*
     *    
     *    *
     *    *    *
     *    *    *     *
       

*/

//Second Approach

import java.util.Scanner;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.println("Enter the no. of rows:: ");
        int N = obj.nextInt();

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= i; j++) {

                    System.out.print("\t" + "*");
                                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

