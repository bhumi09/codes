
/*
     A     B     C
     A     B     C
     A     B     C


*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;

        for (int i = 1; i <= N; i++) {
            
            char ch = 'A';

            for (int j = 1; j <= N; j++) {
            
                System.out.print("\t" + ch);
                ch++;
            }   
            System.out.println();
        }

        System.out.println();
    }
}

