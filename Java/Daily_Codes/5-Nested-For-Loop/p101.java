

/*

     A    B    C    D
     A    B    C
     A    B
     A    

*/

import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();

        for (int i = 1; i <= N; i++) {

        //char ch = (char)(64+i);
            char ch = 'A';
            
            for (int j = 1; j <= N-i+1; j++) {

                    System.out.print("\t" + ch++);
                                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}



