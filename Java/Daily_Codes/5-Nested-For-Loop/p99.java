

/*

     10
     9    8
     7    6    5
     4    3    2     1    

*/
//Take rows = 3

import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();

       int no = (N * N + N)/2;
    
        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= i; j++) {

                    System.out.print("\t" + no--);
                                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}


