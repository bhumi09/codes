
/*
     A    1    B    2
     C    3    D    4
     E    5    F    6
       

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;
        char ch ='A';
        int no = 1;

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= N+1; j++) {

                if(j % 2 == 1){

                    System.out.print("\t" + ch++);
                }
                else{

                    System.out.print("\t" +  no++);
                }
                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

