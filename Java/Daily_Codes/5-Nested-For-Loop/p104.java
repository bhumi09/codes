

/*

    *
    *    #
    *    #    *
    *    #    *    #
    *    #    *    #    *
*/

import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= i; j++) {

                if(j % 2 == 1){

                    System.out.print("\t" + "*");
                }
                else{

                    System.out.print("\t" +  "#");
                }               
            }
            
            System.out.println();
        }

        System.out.println();
    }
}





