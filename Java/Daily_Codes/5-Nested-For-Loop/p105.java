

/*

   A    1    B    2
   C    3    D
   E    4
   F
*/

import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();

        int no = 1;
        char ch = 'A';

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= N-i+1; j++) {

                if(j % 2 == 1){

                    System.out.print("\t" + ch++);
                }
                else{

                    System.out.print("\t" +  no++);
                }               
            }
            
            System.out.println();
        }

        System.out.println();
    }
}





