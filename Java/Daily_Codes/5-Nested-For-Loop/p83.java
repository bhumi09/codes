
/*
     1    2    3
     1    2    3
     1    2    3

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;

        for (int i = 1; i <= N; i++) {
            
            int num = 1;

            for (int j = 1; j <= N; j++) {
            
                System.out.print("\t" + num);
                num++;
            }
            
            System.out.println();
        }

        System.out.println();
    }
}

