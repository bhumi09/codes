
/*
     A     B     C
     D     E     F
     G     H     I

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;       
        char ch = 'A';

        for (int i = 1; i <= N; i++) {
  
            for (int j = 1; j <= N; j++) {
            
                System.out.print("\t" + ch);
                ch++;
            }   
            System.out.println();
        }

        System.out.println();
    }
}

