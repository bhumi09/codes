
/*
     A    1    B    2
     A    1    B    2
     A    1    B    2
     A    1    B    2
       

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 4;

        for (int i = 1; i <= N; i++) {

            char ch ='A';
            int no = 1;
            
            for (int j = 1; j <= N; j++) {

                if(j % 2 == 1){

                    System.out.print("\t" + ch++);
                }
                else{

                    System.out.print("\t" +  no++);
                }
                
            }
            
            System.out.println();
        }

        System.out.println();
    }
}
