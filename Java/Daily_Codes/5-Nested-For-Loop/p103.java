

/*

    1
    4     3
    16    5    36
    49    8    81    10

*/

import java.util.*;

class solution {
    public static void main(String[] args) {

        Scanner obj = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter the no. of rows:: ");
        int N = obj.nextInt();

        System.out.println();

        int no = 1;

        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= i; j++) {

                if(j % 2 == 1){

                    System.out.print("\t" + no * no);
                }
                else{

                    System.out.print("\t" +  no);
                }
                
                 no++;               
            }
            
            System.out.println();
        }

        System.out.println();
    }
}




