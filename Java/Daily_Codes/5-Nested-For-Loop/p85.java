
/*
     3    3    3
     4    4    4
     5    5    5

*/

class solution {
    public static void main(String[] args) {

        System.out.println();

        int N = 3;
        int num = 3;


        for (int i = 1; i <= N; i++) {
            
            for (int j = 1; j <= N; j++) {
            
                System.out.print("\t" + num);
            }
            num++;
            
            System.out.println();
        }

        System.out.println();
    }
}

