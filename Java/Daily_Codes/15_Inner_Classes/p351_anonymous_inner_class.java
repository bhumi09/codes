/* Anonymous Inner Class */

class Demo{

    int x = 10;
    void career(){

        System.out.println("Engineering");
    }
}
class Client{
    public static void main(String[] args) {
        
        Demo obj = new Demo(){

            void career(){

                System.out.println("IIT");
            }
        };

        obj.career();
    }
}