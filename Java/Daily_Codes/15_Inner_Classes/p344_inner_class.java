class Parent {

    int x = 10;
    static int y = 20;

    Parent() {

        System.out.println("In Parent constructor");
    }

    void m1() {

        System.out.println("In m1");
    }
    static void m2(){

        System.out.println("In m2");
    }
}
class Child extends Parent{

    int a = 10;
    static int b = 20;

    Child() {

        System.out.println("In Child constructor");
    }

    void m1() {

        System.out.println("In child m1");
    }
    static void m3(){

        System.out.println("In child m3");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Parent();
        obj.m1();
        obj.m2();

        Child obj2 = new Child();
        obj2.m1();
        obj2.m3();

        Parent obj3 = new Child();
        obj3.m1();
        obj3.m2();
        //obj3.m3();
    
    }
}