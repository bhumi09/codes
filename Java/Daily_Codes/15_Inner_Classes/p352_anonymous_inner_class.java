/* Anonymous Inner Class */

class Demo{

    void career(){

        System.out.println("Engineering");
    }
}
class Client{
    public static void main(String[] args) {
        
        Demo obj = new Demo(){

            void career(){

                System.out.println("IIT");
                //fun();
            }
            void fun(){
                
                System.out.println("In fun");
            }
        };

        obj.career();
        //obj.fun();                                     error: cannot find symbol
    }
}