//To access and print multiple data of same class, for-each loop is used.

class ForEachDemo{

    public static void main(String[] args) {
        
        System.out.println();

        int arr[] = {10, 20, 30, 40, 50};

        //Printing the array using for loop
        for(int i = 0; i< arr.length; i++){

            System.out.print(arr[i] + "   ");
        }

        System.out.println();

         //Printing the array using for - each loop
         for(int x : arr){

            System.out.print(x + "   ");
         }
         System.out.println();
    }
}