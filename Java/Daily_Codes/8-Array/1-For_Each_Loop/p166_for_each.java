//Character array

class ForEachDemo{

    public static void main(String[] args) {
        
        char arr[] = {'A', 'B', 'C'};

        System.out.println(arr);                //Prints the string of whole character array

        for(char x : arr){

            System.out.println(x);              //Prints each element from array 
        }
    }
}