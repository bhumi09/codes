//Accessing array elements

class solution{

    public static void main(String[] args) {
        
        System.out.println();  

        //Integer Array
        int arr[] = {10, 20, 30, 40};

        //Float Array
        float arr1[] = {10.5f, 20.5f, 30.5f};

        //Character Array
        char arr2[] = {'A', 'B', 'C'};

        //Boolean Array
        boolean arr3[] ={true, false};

        //Accessing array elements

        System.out.println("Integer Array Elements");
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
        System.out.println(arr[3]);

        System.out.println();

        System.out.println("Float Array Elements");
        System.out.println(arr1[0]);
        System.out.println(arr1[1]);
        System.out.println(arr1[2]);

        System.out.println();

        System.out.println("Character Array Elements");
        System.out.println(arr2[0]);
        System.out.println(arr2[1]);
        System.out.println(arr2[2]);

        System.out.println();

        System.out.println("Boolean Array Elements");
        System.out.println(arr3[0]);
        System.out.println(arr3[1]);

        System.out.println();
    }
}