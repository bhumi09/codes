/*
    Write a program to  take size of array from user and also take the integer elements from user.
    Print sum of all array elements.

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int sum = 0;

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
                sum = sum + arr[i];
        
        }

        System.out.println("\n" + "Sum of array elements elements= " + sum + "\n");
    }
 }