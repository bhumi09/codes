
//Passing arguments to method

class FunDemo{

    public static void main(String[] args) {

        FunDemo fd = new FunDemo();
        fd.fun(10);             //error: method fun in class FunDemo cannot be applied to given types;
                                /*
                                    required: no arguments
                                    found:    int
                                    reason: actual and formal argument lists differ in length
                                 */
    }
    void fun(){

        System.out.println("In fun");
    }
}