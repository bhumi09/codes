
//Try run code

class solution{

    public static void main(String[] args) {

        System.out.println();
        
        float f = 10.5f;
        float x = 10.5f;

        System.out.println("f => " + System.identityHashCode(f));
        System.out.println("x => " + System.identityHashCode(x));

        System.out.println();
    }
}