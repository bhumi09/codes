//Try run code
//Passing argument to method 

class FunDemo{

    static void fun(int x, double y){

       System.out.println(x);
       System.out.println(y);
    }

    public static void main(String[] args) {
        
      
       fun(10, 20.5);
        System.out.println(fun(10, 20.5));      //error: 'void' type not allowed here

    }
}