// Different ways to declare array

class solution{

    public static void main(String[] args) {
        
        int arr[];

        //this type of declaration is not allowed in java.
        //int arr[5];  

        //Correct way to declare array in Java.
        int arr1[] = new int[5];
          
        int arr2[] = new int[];       //error: array dimension missing
    }
}
