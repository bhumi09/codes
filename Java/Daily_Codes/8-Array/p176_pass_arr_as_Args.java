//Passing array as an argument

class FunDemo{

    static  void fun(int arr[]){

        System.out.println("In fun method...");
        for(int x : arr){

            System.out.println(x + "   ");
        }
       
        for(int i = 0; i < arr.length; i++){

            arr[i] = arr[i] + 50;
        }

        System.out.println("\n");
        
    }

    public static void main(String[] args) {
        
        int arr[] = {50, 100, 150};
    
        fun(arr);
        
        System.out.println("After calling fun..");
        for(int x : arr){

            System.out.println(x + "   ");
        }
    }
}
