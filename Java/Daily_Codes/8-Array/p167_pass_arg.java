
//Passing argument to method

class FunDemo{

    public static void main(String[] args) {
        
        fun();              //error: non-static method fun() cannot be referenced from a static context
    }
    void fun(){

        System.out.println("In fun");
    }
}