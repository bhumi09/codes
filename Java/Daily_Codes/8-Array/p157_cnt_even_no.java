/*
    Write a program to  take size of array from user and also take the integer elements from user.
    Print count of even elements.

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int cnt= 0;

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
               if(arr[i] % 2 == 0){
                
                cnt++;
            }
        
        }

        System.out.println("\n" + "Count of even element = " + cnt+ "\n");
    }
 }