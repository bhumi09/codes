//Try run codes of Array declaration

class solution{

    public static void main(String[] args) {
        
        int arr1[] = new int[4];        //Correct declaration
        int arr2[] = new int[];         //error: array dimension missing
    }
}
