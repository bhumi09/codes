
//Try run code

class FunDemo{

    static void fun(int x, int y){

        System.out.println("\n x = " + x);
        System.out.println("y = " + y);

        x = x + 10;
        y = y + 10;

        System.out.println("\n x = " + x);
        System.out.println("y = " + y);
    }
    public static void main(String[] args) {
        
        int x = 10;
        int y = 20;

        System.out.println("\n x = " + x);
        System.out.println("y = " + y);

        fun(x, y);

        System.out.println("\n x = " + x);
        System.out.println("y = " + y);

    }
}