/*
    Write  a program to print the array. Take a size of array and input from user using Scanner class
 */

 import java.util.*;
 class solution{

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\n Enter the size of array:: ");
        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println();
        System.out.println("Enter Array elements:  ");

        for(int i = 0; i < arr.length; i++){

            arr[i] = sc.nextInt();
        }

        System.out.println("\n" + "Array elements are::  ");

        for(int i = 0; i< arr.length; i++){

            System.out.print("\t" + arr[i]);
        }
        
       System.out.println("\n"); 
    }
 }