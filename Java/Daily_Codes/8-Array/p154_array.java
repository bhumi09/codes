/*
    Write  a program to print the array. Take a input from user using Scanner class
 */

 import java.util.*;

import javax.swing.plaf.nimbus.AbstractRegionPainter;
 class solution{

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int arr[] = new int[5];

        System.out.println();
        System.out.println("Enter Array elements:  ");

        for(int i = 0; i < arr.length; i++){

            arr[i] = sc.nextInt();
        }

        System.out.println("\n" + "Array elements are::  ");

        for(int i = 0; i< arr.length; i++){

            System.out.print("\t" + arr[i]);
        }
        
        
    }
 }