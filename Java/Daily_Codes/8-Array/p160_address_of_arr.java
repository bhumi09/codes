
//Address of array
//Use of IdentityHashCode

class solution{

    public static void main(String[] args) {
        
        System.out.println();

        int arr1[] ={100, 200, 300, 400};
        byte arr2[] = {1, 2, 3, 4};
        short arr3[] = {10, 20, 30, 40};
        boolean arr4[] = {true, false, true};
        //float arr5[] = {10.5, 20.5, 30.5, 40.5};        //error: incompatible types: possible lossy conversion from double to float
       
        double arr6[] = {10.5, 20.5, 30.5, 40.5};
        double arr7[] = { 10d, 20d, 30d, 40d};
        float arr8[] = {10.5f, 20.5f, 30.5f, 40.5f};



        System.out.println("arr1 =>   " + arr1);
        System.out.println("arr2 =>   " +arr2);
        System.out.println("arr3 =>   " +arr3);
        System.out.println("arr4 =>   " +arr4);
        //System.out.println("arr5 =>   " +arr5);
        System.out.println("arr6 =>   " +arr6);
        System.out.println("arr7 =>   " +arr7);
        System.out.println("arr8 =>   " +arr8);


        System.out.println();
    }
}