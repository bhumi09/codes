
//Passing argument to method and returning values
class FunDemo{

    int fun(int x){

        int val = x + 50;
        return val;
    }

    public static void main(String[] args) {
        
        FunDemo fd = new FunDemo();
        int ret = fd.fun(10);

        System.out.println(ret);
    }
}