//Address of array
//Use of IdentityHashCode

class solution{

    public static void main(String[] args) {
        
        int arr[] = {10, 20, 30, 40, 50};
        int arr1[] = {10, 20, 30, 40, 50};

        System.out.println();

        System.out.println("arr =>  " + arr);
        System.out.println("arr1 =>  " + arr1);

        System.out.println();

        //Accessing address of first element of both array

        System.out.println("arr[0] =>  " + System.identityHashCode(arr[0]));
        System.out.println("arr1[0] =>  " + System.identityHashCode(arr1[0]));

        System.out.println();
    }
}