/* Command Line Arguments in JAVA */
//Printing IdentityHashCode

class solution{

    public static void main(String[] args) {
        
        String arr[] = {"Ashish" , "Ashish"};

        System.out.println("arr[0] => " + System.identityHashCode(arr[0]));
        System.out.println("arr[1] => " + System.identityHashCode(arr[1]));

        System.out.println();

        System.out.println("args[0] => " + args[0]);
        System.out.println("args[0] => " + args[1]);

        System.out.println();

        System.out.println("args[0] => " + System.identityHashCode(args[0]));
        System.out.println("args[0] => " + System.identityHashCode(args[1]));

        System.out.println();


    }
}