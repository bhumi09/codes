/* Command Line Arguments in JAVA */

class solution{

    void fun(int arr[]){

        arr[1] = 70;
        arr[2] = 80;
    }
    public static void main(String[] args) {

        int arr[] = {10, 20, 30, 40, 50};

        System.out.println("arr[0] =>  " + System.identityHashCode(arr[0]));
        System.out.println("arr[1] =>  " + System.identityHashCode(arr[1]));
        System.out.println("arr[2] =>  " + System.identityHashCode(arr[2]));
        System.out.println("arr[3] =>  " + System.identityHashCode(arr[3]));

        solution obj = new solution();
        obj.fun(arr);

        for(int x : arr){

            System.out.println(x);
        }

        int x = 10;
        int y = 20;
        System.out.println("x =>  " + System.identityHashCode(x));
        System.out.println("y =>  " + System.identityHashCode(y));
        
    }
}