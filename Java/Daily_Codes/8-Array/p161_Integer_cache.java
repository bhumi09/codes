//Integer Cache 

class IntegerCache{

    public static void main(String[] args) {
        
        int x = 10;
        int y = 20;

        System.out.println("X =  " + System.identityHashCode(x));
        System.out.println("Y =  " + System.identityHashCode(y));

        System.out.println();

        int a = 10;
        int b = 10;

        System.out.println("a =  " + System.identityHashCode(a));
        System.out.println("b =  " + System.identityHashCode(b));

        System.out.println("\n");

        int p = 10;
        int q = 10;
        Integer r = 10;

        System.out.println("p =  " + System.identityHashCode(p));
        System.out.println("q =  " + System.identityHashCode(q));
        System.out.println("r =  " + System.identityHashCode(r));

        System.out.println("\n");

        Integer s = new Integer(10);
        System.out.println("s =  " + System.identityHashCode(s));



    }
}