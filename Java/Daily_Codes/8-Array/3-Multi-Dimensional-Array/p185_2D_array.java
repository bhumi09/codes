 //2 Dimensional array
 //IdentityHashCode of Elements

 class solution{

    public static void main(String[] args) {
        
        int arr[][] = new int[2][2];

        arr[0][0] = 10;
        arr[0][1] = 10;
        arr[1][0] = 10;
        arr[1][1] = 10;

        System.out.println();

        System.out.println("arr[1][1] =>  " + arr[1][1]);
        System.out.println("arr[1] =>  " +arr[1]);
        System.out.println("arr =>  " +arr);
        System.out.println("arr[0] =>  " +arr[0]);

        System.out.println();
    }
 }