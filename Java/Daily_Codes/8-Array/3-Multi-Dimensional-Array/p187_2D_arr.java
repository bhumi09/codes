//2D Array taking input from user

import java.io.*;
class solution{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();
        
        System.out.print("Enter the no. of rows::  ");
        int rows = Integer.parseInt(br.readLine());

        System.out.print("Enter teh no. of columns::  ");
        int cols = Integer.parseInt(br.readLine());

        int arr[][] = new int[rows][cols];

        System.out.println("Enter array elements::  ");
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr.length; j++){

                arr[i][j] = Integer.parseInt(br.readLine());
            }
        }

        System.out.println("\n Array elements are:: ");
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr.length; j++){

                System.out.print("    " + arr[i][j]);
            }
            System.out.println();
        }        
        System.out.println();
    }
}
