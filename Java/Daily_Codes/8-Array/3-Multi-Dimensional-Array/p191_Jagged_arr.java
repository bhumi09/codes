/*  Ways to declare Jagged array */

class solution{

    public static void main(String[] args) {
        
        //First way
        int arr1[][] = {{1, 2, 3}, {4, 5}, {6}};

        //Second way
        int arr[][] = new int [3][];

        arr[0] = new int[] {1, 2, 3};
        arr[1] = new int[] {4, 5};
        arr[2] = new int[] {7};


    }
}