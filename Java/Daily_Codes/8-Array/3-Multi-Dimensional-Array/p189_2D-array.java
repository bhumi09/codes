
//2D and Jagged array

class solution {

    public static void main(String[] args) {

        int arr1[][] = new int[3][3]; // 2D Array
        int arr2[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; // Jagged Array

        System.out.println();
        // Printing arr1

        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {

                System.out.print("    " + arr1[i][j]);
            }
            System.out.println();
        }

        //Assigning values to arr1
        int x = 0;

        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {

                arr1[i][j] = 10 + x;
                x++;
            }
            System.out.println();
        }
         
        // Printing arr1

         for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {

                System.out.print("    " + arr1[i][j]);
            }
            System.out.println();
        }
        System.out.println();

    }
}