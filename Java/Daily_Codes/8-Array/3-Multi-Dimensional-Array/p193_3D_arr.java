//Print 3 Dimensional Array

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int arr[][][] = { { { 1, 2, 3 }, { 4, 5, 6}, { 7, 8, 9 } },
                { { 10, 11, 12 }, { 13, 14, 15 }, { 16, 17, 18} } };

        System.out.println("\n Array elements are::  ");

        /* 
        //For-each loop
        for (int[][] xarr : arr) {
            for (int[] y : xarr) {
                for (int z : y) {

                    System.out.print("    " + z);
                }
                System.out.println();
            }
        }
*/

        //for loop
        for(int i = 0; i< arr.length; i++){
            for(int j = 0; j < arr[i].length; j++){
                for(int k = 0; k < arr[i].length; k++){

                    System.out.print("  " + arr[i][j][k]);
                }
                System.out.println();
            }
        }
        System.out.println();
    }
}