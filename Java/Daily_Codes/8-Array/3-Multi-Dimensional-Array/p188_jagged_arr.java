//Jagged Array print using For-Each loop

class solution{
    public static void main(String[] args){

        System.out.println();

        int arr[][] = {{10, 20}, {30}, {40, 50}};

        System.out.println("\n Array elements are:: ");
        for(int xarr[] : arr){
            for(int x : xarr){

                System.out.print( "   " + x);
            }
            System.out.println();
        }       
        System.out.println();
    }
}
