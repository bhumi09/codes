/*
    Write  a program to print the array. Take a input from user using BufferdReader.
 */

import java.io.*;
 class solution{

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int arr[] = new int[5];

        System.out.println();
        System.out.println("Enter array elements:: ");
        
        for(int i = 0; i< arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());
        }

        System.out.println("\n" + "Array elements are::  ");

        for(int i = 0; i< arr.length; i++){

            System.out.print("\t" + arr[i]);
           
        }
        System.out.println();
        
    }
 }