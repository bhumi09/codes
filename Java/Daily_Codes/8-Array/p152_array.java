//Accessing array elements

class solution{

    public static void main(String[] args) {
        
        int arr[] = {10, 20, 30, 40, 50};

        System.out.println();
        System.out.println("Array elements are :: ");

        System.out.println();

        for(int i = 0; i < 5; i++){

            System.out.print("\t" + arr[i]);
        }

        System.out.println();
    }
}