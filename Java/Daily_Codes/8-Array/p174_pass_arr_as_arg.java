//Passing array as an argument

class FunDemo{

    static  void fun(int xarr[]){

        System.out.println("In fun method...");
        for(int x : xarr){

            System.out.println(x + "   ");
        }

        System.out.println("\n"); 
    }

    public static void main(String[] args) {
        
        int arr[] = {10, 20, 30};

        System.out.println("In main...");
        for(int x : arr){

            System.out.println(x + "   ");
        }
      

        fun(arr);
    }
}