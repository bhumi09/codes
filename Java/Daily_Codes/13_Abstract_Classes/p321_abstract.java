/* Abstract Class */

abstract class Parent{                               

    void Career(){

        System.out.println("Engineering");
    }
    abstract void admission();
} 
class Child extends Parent{
    
    void admission(){

        System.out.println("COEP");
    }
}
class Client{
    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.Career();
        obj.admission();

    }
}