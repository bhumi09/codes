 /* Real Timme Example- Abstract Class */

abstract class SujataMastani{                               

    void items(){

        System.out.println("Mango Mastani");
    }
    abstract void price();
} 
class Francise_1 extends SujataMastani{
    
    void price(){

        System.out.println("90 Rs.");
    }
}
class Customer{
    public static void main(String[] args) {
        
        SujataMastani obj = new Francise_1();
        obj.items();
        obj.price();

    }
} 
    

