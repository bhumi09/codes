/* Singleton Design Pattern */

class Singleton{

    static Singleton obj = new Singleton();
    private Singleton(){

        System.out.println( "Constructor");
    }
    static Singleton getObject(){

        return obj;
    }
}
class Client{
    public static void main(String[] args) {
        
        Singleton obj2 = Singleton.getObject();
        System.out.println("obj2: " + obj2);

        Singleton obj3 = Singleton.getObject();
        System.out.println("obj3: " + obj3);

        Singleton obj4 = Singleton.getObject();
        System.out.println("obj4: " + obj4);
    }
}