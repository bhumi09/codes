 /* Abstract Class */

abstract class Parent{                               

    void Career(){

        System.out.println("Engineering");
    }
    void admission(){

        System.out.println("COEP");
    }
} 
class Child extends Parent{
    
    void admission(){

        System.out.println("VJTI");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Child();
        obj.Career();
        obj.admission();

    }
} 
    

