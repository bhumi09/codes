/* Abstract Class */

class Parent{                               //error: Parent is not abstract and does not override abstract method admission() in Parent

    void Career(){

        System.out.println("Engineering");
    }
    abstract void admission();
}