 /* Abstract Class */

abstract class Parent{                               

    void Career(){

        System.out.println("Engineering");
    }
    abstract void admission();
} 
class Client{
    public static void main(String[] args) {
        
        Parent obj = new Parent();              //error: Parent is abstract; cannot be instantiated

    }
}