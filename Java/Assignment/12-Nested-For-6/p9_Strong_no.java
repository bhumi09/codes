
/*
    Write a program to take range as input from the user and print the series of Strong numbers.
            
            Input::
                    Enter start:: 1
                    Enter end  :: 150

            Output::
            Strong numbers between 1 and 150::  
            1, 2, 145
            
 */

 import java.io.*;

 class solution {
 
     void isStrongNo(int start, int end) {
 
         System.out.print("\nStrong numbers between " + start + " and " + end + ":: ");
 
         for (int i = start; i <= end; i++) {
             
            int sum = 0;
             for (int j = i; j != 0; j = j / 10) {
 
                 int rem = j % 10;
                 int fact = 1;
               
                 for(int k = 2; k <= rem; k++){

                    fact = fact * k;
                 }
                 sum = sum + fact;
 
             }
        
             if(sum == i){

                System.out.print("   " + i);
             }
         }
         System.out.println("\n");
 
     }
 
     public static void main(String[] args) throws IOException {
 
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
         solution obj = new solution();
 
         System.out.print("\n" + "Enter start::  ");
         int start = Integer.parseInt(br.readLine());
 
         System.out.print("Enter end::  ");
         int end = Integer.parseInt(br.readLine());
 
         obj.isStrongNo(start, end);
 
         br.close();
 
     }
 }