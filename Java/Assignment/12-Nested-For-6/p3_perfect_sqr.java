/*
    Write a program to take range as input from the user and print perfect squares between range.
            
            Input::
                    Enter start:: 1
                    Enter end  :: 100

            Output::
            Perfect squares between 1 and 100::  
          

 */

 import java.io.*;
 class solution{

    void isPerfectSqr(int start, int end){
       
        System.out.print("\nPerfect squares between " + start + " and " + end + ":: ");
    
        for(int i = start; i * i<= end; i++){
            
            System.out.print("  " + i*i);
       
        }
        System.out.println("\n");

    }

    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution obj = new solution();

        System.out.print("\n" + "Enter start::  ");
        int start = Integer.parseInt(br.readLine());

        System.out.print("Enter end::  " );
        int end = Integer.parseInt(br.readLine());

        obj.isPerfectSqr(start, end);

        br.close();
        
    }
 }