
/*
    Write a program to take range as input from the user and print perfect numbers.
            
            Input::
                    Enter start:: 1
                    Enter end  :: 30

            Output::
            Perfect numbers between 1 and 300::  
            6    28
          

 */

 import java.io.*;
 class solution{

    void isPerfectNo(int start, int end){
       
        System.out.print("\nPerfect numbers between " + start + " and " + end + ":: ");

        for(int i = start; i <= end; i++){

            int sum = 0;
            for(int j = 1; j < i; j++){

                if(i % j == 0){

                    sum = sum + j;

                }
            }
            
            if(sum == i){

                System.out.print("  " + i);
            }
        }
        System.out.println("\n");

    }

    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution obj = new solution();

        System.out.print("\n" + "Enter start::  ");
        int start = Integer.parseInt(br.readLine());

        System.out.print("Enter end::  " );
        int end = Integer.parseInt(br.readLine());

        obj.isPerfectNo(start, end);

        br.close();
        
    }
 }