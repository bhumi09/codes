/*
    Write a program to print the numbers divisible by 5 from 1 to 50 and the number is even also 
    print the count of even numbers.

            Input::
                    Enter lower limit:: 1
                    Enter upper limit:: 50

            Output:: 10, 20, 30, 40, 50
                     Count = 5

 */

 import java.io.*;
 class solution{

    void isDivisible(int lowerLimit, int upperLimit){
       
        System.out.print("\nNumbers divisible by 5 and 2 are:: ");
        int cnt = 0;

        for(int i = lowerLimit; i <= upperLimit; i++){

            if(i % 5 == 0 && i % 2 == 0){

                System.out.print("  " + i);
                cnt++;
            }
        }
        System.out.println("\nCount of even numbers::  " + cnt + "\n");

    }

    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution obj = new solution();

        System.out.print("\n" + "Enter lower limit::  ");
        int lowerLimit = Integer.parseInt(br.readLine());

        System.out.print("Enter upper limit::  " );
        int upperLimit = Integer.parseInt(br.readLine());

        obj.isDivisible(lowerLimit, upperLimit);

        br.close();
        
    }
 }