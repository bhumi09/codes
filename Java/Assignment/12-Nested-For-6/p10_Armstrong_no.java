

/*
    Write a program to take range as input from the user and print the Armstrong numbers.
            
            Input::
                    Enter start:: 1
                    Enter end  :: 1650

            Output::
            Armstrong numbers between 1 and 1650::  
            1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370, 371, 407, 1634
            
 */

 import java.io.*;

 class solution {
 
     void isArmstrongNo(int start, int end) {
 
         System.out.print("\nArmstrong numbers between " + start + " and " + end + ":: ");
 
         for (int i = start; i <= end; i++) {
             
            int cnt = 0;
             for (int j = i; j != 0; j = j / 10) {
                cnt++;
 
             }

             int sum = 0;
             for(int j = i; j != 0; j = j / 10){

                int rem = j % 10;
                int mul = 1;

                for(int k = 1; k <= cnt; k++){

                    mul = mul * rem;
                }
                sum = sum +mul;
             }
        
             if(sum == i){

                System.out.print("   " + i);
             }
         }
         System.out.println("\n");
 
     }
 
     public static void main(String[] args) throws IOException {
 
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
         solution obj = new solution();
 
         System.out.print("\n" + "Enter start::  ");
         int start = Integer.parseInt(br.readLine());
 
         System.out.print("Enter end::  ");
         int end = Integer.parseInt(br.readLine());
 
         obj.isArmstrongNo(start, end);
 
         br.close();
 
     }
 }