/*
    Write a program to take range as input from the user and print composite numbers.
            
            Input::
                    Enter start:: 1
                    Enter end::   20

            Output::
            Composite numbers between 1 and 20::  
            4, 6, 8, 9, 10, 12, 14, 15,16, 18, 20

 */

 import java.io.*;
 class solution{

    void isComposite(int start, int end){
       
        System.out.print("\nComposite numbers between " + start + " and " + end + ":: ");
    
        for(int i = start; i <= end; i++){
           
            int cnt = 0;
            for(int j = 1; j <= i; j++){

                if(i % j == 0){

                    cnt++;
                }
                if(cnt > 2){
                    break;
                }
            }
            if(cnt > 2){
                System.out.print("  " + i);
            }
        }
        System.out.println("\n");

    }

    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution obj = new solution();

        System.out.print("\n" + "Enter start::  ");
        int start = Integer.parseInt(br.readLine());

        System.out.print("Enter end::  " );
        int end = Integer.parseInt(br.readLine());

        obj.isComposite(start, end);

        br.close();
        
    }
 }