/*
    Write a program to take 5 numbers as input from the user and print the 
    count of digits in those numbers.
            
            Input::
                    Enter 'n' numbers::
                    5
                    The digit count in 5 is 1

                    25
                    The digit count in 25 is 2

                    225
                    The digit count in 225 is 3

                    
 */

 import java.io.*;
 class solution{

    void CntDigit(int num){
       
        int cnt = 0;
        for(int i = num; i != 0; i = i /10){

            cnt++;
        }

        System.out.println("The digit count in " + num + " is " + cnt);

    }

    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution obj = new solution();

        System.out.print("\n" + "Enter number of elements for those you want to count digit::  ");
        int N = Integer.parseInt(br.readLine());

        for(int i = 1; i <= N; i++){

            System.out.print("\nEnter number:: ");
            int num = Integer.parseInt(br.readLine());
            obj.CntDigit(num);
        }
    
        br.close();
        
    }
 }