

/*
    Write a program to take range as input from the user and print the palindrome numbers.
            
            Input::
                    Enter start:: 100
                    Enter end  :: 250

            Output::
            palindrome numbers between 100 and 250::  
            101, 11, 121, 131, 141, 151, 161, 171, 181, 191, 202, 212, 222
            
 */

 import java.io.*;

 class solution {
 
     void isPalindromeNo(int start, int end) {
 
         System.out.print("\npalindrome numbers between " + start + " and " + end + ":: ");
 
         for (int i = start; i <= end; i++) {
 
             int rev = 0;

 
             for (int j = i; j != 0; j = j / 10) {
 
                 int rem = j % 10;
                 rev = rev * 10 + rem;
 
             }
        
             if(rev == i){

                System.out.println(rev);
             }
         }
         System.out.println("\n");
 
     }
 
     public static void main(String[] args) throws IOException {
 
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
         solution obj = new solution();
 
         System.out.print("\n" + "Enter start::  ");
         int start = Integer.parseInt(br.readLine());
 
         System.out.print("Enter end::  ");
         int end = Integer.parseInt(br.readLine());
 
         obj.isPalindromeNo(start, end);
 
         br.close();
 
     }
 }