
/*
    Write a program to take range as input from the user and print the reverse of all numbers.
            
            Input::
                    Enter start:: 100
                    Enter end  :: 200

            Output::
            Reverse numbers between 100 and 200::  
            
 */

import java.io.*;

class solution {

    void isReverseNo(int start, int end) {

        System.out.print("\nReverse numbers between " + start + " and " + end + ":: ");

        for (int i = start; i <= end; i++) {

            int rev = 0;

            for (int j = i; j != 0; j = j / 10) {

                int rem = j % 10;
                rev = rev * 10 + rem;

            }
            System.out.println(rev);
        }
        System.out.println("\n");

    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution obj = new solution();

        System.out.print("\n" + "Enter start::  ");
        int start = Integer.parseInt(br.readLine());

        System.out.print("Enter end::  ");
        int end = Integer.parseInt(br.readLine());

        obj.isReverseNo(start, end);

        br.close();

    }
}