
/*
   F
   E    1
   D    2     E
   C    3     D     4
   B    5     C     6     D
   A    7     B     8     C     9
    
 */

class solution {

    public static void main(String[] args) {

        System.out.println();

        int rows = 6;
        int num = 1;
        char ch1 = 'F';

        for (int i = 1; i <= rows; i++) {

            char ch = ch1;

            for (int j = 1; j <= i; j++) {

                if (j % 2 == 1) {

                    System.out.print("\t" + ch);
                    ch++;
                } else {

                    System.out.print("\t" + num);
                    num++;
                }
            }
            ch1--;
            System.out.println();
        }
        System.out.println();
    }
}
