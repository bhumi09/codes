
/*
   10
   I     H
   7     6     5
   D     C     B     A
    
 */

 class solution {

    public static void main(String[] args) {

        System.out.println();

        int rows = 4;
        int num = 10;
        char ch = 'J';

        for (int i = 1; i <= rows; i++) {

            for (int j = 1; j <= i; j++) {

                if (i % 2 == 0) {

                    System.out.print("\t" + ch);
                } else {

                    System.out.print("\t" + num);
                }
                ch--;
                num--;
                
            }
            
            System.out.println();
        }
        System.out.println();
    }
}

