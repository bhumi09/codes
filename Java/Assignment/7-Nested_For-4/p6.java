
/*
    1
    2     3
    3     4     5
    4     5     6     7
    
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        int num = (rows * (rows + 1)/2);
        
        for(int i =1; i <= rows; i++){
            num = i;

            for(int j = 1; j <= i; j++){

                System.out.print("\t" + num);
                num++;
            }
            num--;
           
            System.out.println();
        }
        System.out.println();
    }
 }



