
/*
    1    2    3    4
    2    3    4
    3    4
    4
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        int no = (rows * (rows +1)/2);
        
        for(int i =1; i <= rows; i++){

            no = i;

            for(int j = 1; j <= rows-i+1; j++){

                System.out.print("\t" + no);
                no++;
            }
            
           
            System.out.println();
        }
        System.out.println();
    }
 }



