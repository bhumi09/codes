
/*
    10
    10    9
    9     8     7
    7     6     5     4
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        int no = (rows *(rows+1)/2);
        
        for(int i =1; i <= rows; i++){

            for(int j = 1; j <= i; j++){

                System.out.print("\t" + no);
                no--;
            }
            no++;
           
            System.out.println();
        }
        System.out.println();
    }
 }



