
/*
   1
   8    9
   27   16    125
   64   25    216   49
    
 */

 class solution {

    public static void main(String[] args) {

        System.out.println();

        int row = 4;
        int num = (row *(row + 1)/2);

        for (int i = 1; i <= row; i++) {
            
            num = i;

            for (int j = 1; j <= i; j++) {

                if (j % 2 == 1) {

                    System.out.print("\t" + num*num*num);

                } else {

                    System.out.print("\t" + num*num);
                }
                
                num++;
                
            }
            
            System.out.println();
        }
        System.out.println();
    }
}

