
/*
   A    B    C    D
   B    C    D
   C    D
   C

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        char ch = 'A';
        
        for(int i =1; i <= rows; i++){

            //ch = i;
            char ch1 = ch;

            for(int j = 1; j <= rows-i+1; j++){

                System.out.print("\t" + ch1);
                ch1++;
            }       
            ch++;
            System.out.println();
        }
        System.out.println();
    }
 }




