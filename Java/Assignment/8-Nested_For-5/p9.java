/*
    Write a program to take a number as input and print the addition of factorials of each digit
    from that number.
      
        Input:: 1234
        Output:: Addition of factorials od each digit from 1234 = 33

 */

 import java.io.*;

 class solution{

    public static void main(String[] args)throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the number:: ");
        int num = Integer.parseInt(br.readLine());

        System.out.println();

        int sum = 0;
        
        for(int i = num; i != 0; i /= 10){

            int rem = i % 10;
            int fact = 1;
           
            for(int j = 1; j <= rem; j++){

                fact = fact * j;
            }
            sum = sum + fact;
        }

        System.out.print("Addition of factorials of each digit of " + num + " = " + sum + "\n");
        System.out.println();
    }
}



        