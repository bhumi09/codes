/*
  Take the number of rows from user.

        D4   C3   B2   A1
        A1   B2   C3   D4
        D4   C3   B2   A1
        A1   B2   C3   D4
 */

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the no. of rows:: ");
        int rows = Integer.parseInt(br.readLine());

        System.out.println();

    
        for (int i = 1; i <= rows; i++) {

            int no = rows;
            char ch1 = (char) (64 + rows);

            for (int j = 1; j <= rows; j++) {

                if (i % 2 == 1) {

                    System.out.print("\t" + ch1);
                    ch1--;
                    System.out.print(no);
                    no--;
                } else {

                    System.out.print("\t" + (char)(64+j));
                    //ch++;
                    System.out.print(j);
                    //no1++;

                }
            }
            System.out.println();
        }

        System.out.println();
    }
}