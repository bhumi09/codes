/*
  Take the number of rows from user.

       5    4    3    2     1
       8    6    4    2
       9    6    3
       8    4
       5
 */

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the no. of rows:: ");
        int rows = Integer.parseInt(br.readLine());

        System.out.println();

         int num = rows;

        for (int i = 1; i <= rows; i++) {

            int temp = num * i;

            for (int j = rows; j >= i; j--) {

                System.out.print("\t" + temp);
                temp = temp - i;
            }
            num--;
            System.out.println();
        }

        System.out.println();
    }
}