/*
  Take the number of rows from user.

        #    =    =    =    =
        =    #    =    =    =
        =    =    #    =    =
        =    =    =    #    =
        =    =    =    =    #
 */

 import java.io.*;

 class solution {
 
     public static void main(String[] args) throws IOException {
 
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
         System.out.println();
 
         System.out.print("\t Enter the no. of rows:: ");
         int rows = Integer.parseInt(br.readLine());
 
         System.out.println();
 
     
         for (int i = 1; i <= rows; i++) {
 
             for (int j = 1; j <= rows; j++) {
 
                 if (i == j) {
 
                     System.out.print("\t" + "#");
                     
                 } else {
 
                     System.out.print("\t" + "=");
                 }
             }
             System.out.println();
         }
 
         System.out.println();
     }
 }