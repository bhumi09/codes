/*
  Take 2 characters, if these characters are equal then print them as it is if they are eual.
  If they are unequal then print their difference.

        input:: a    p
        Output::  Difference between a  and  p is 15.
 */

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the first character:: ");
        int ch = br.read();

        br.skip(1);

        System.out.print("\t Enter the second character:: ");
        int ch1 = br.read();

        System.out.println();

        if(ch == ch1){
            
            System.out.println(ch + "  and  " + ch1 + " both are equal");
        }
        else if(ch < ch1){

            int ans = ch1 - ch;
            System.out.println("Difference between " + ch + "  and  " + ch1 + " is :: " + ans);
        }
        else{
            int ans = ch - ch1;
            System.out.println("Difference between " + ch + "  and  " + ch1 + " is :: " + ans);
            
        }
        
    }
}