/*
    Take a input from user.

    Rows = 8

    $
    @    @
    &    &    &
    #    #    #    #
    $    $    $    $    $
    @    @    @    @    @    @
    &    &    &    &    &    &    & 
    #    #    #    #    #    #    #    #
 */

 import java.io.*;
 class solution{

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the no. of rows:: ");
        int rows = Integer.parseInt(br.readLine());

        System.out.println();
        int num = 1;

        for(int i = 1; i <= rows; i++){
            for(int j = 1; j <= i; j++){

                if(num == 1){

                    System.out.print("\t" + "$");
                }
                else if( num == 2){

                    System.out.print("\t" + "@");
                }
                else if(num == 3){

                    System.out.print("\t" + "&");
                }
                else{
                    System.out.print("\t" + "#");
                }
            }
            if(num == 4){
                num = num -4;
            }
            num++;
            System.out.println();
        }
        System.out.println();
        
        br.close();
    }
 }