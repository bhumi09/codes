/*
  Write a program to print all even no. in reverse order 
  and odd no. in standard way within range.

        input::  Enter start no :: 2
                 Enter end no ::   9

        Output::  8    6    4   2
                  3    5    7   9

*/

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the start no:: ");
        int start = Integer.parseInt(br.readLine());

        System.out.print("\t Enter the end no:: ");
        int end = Integer.parseInt(br.readLine());

        System.out.println();

        for (int i = end; i >= start; i--) {

            if (i % 2 == 0) {

                System.out.print("\t" + i);
            }
        }

        System.out.println();

        for (int j = start; j <= end; j++) {

            if (j % 2 != 0) {

                System.out.print("\t" + j);
            }
        }

        System.out.println();

    }
}
