/*
  Take the number of rows from user.

        0
        1   1
        2   3    5
        8   13   21   34
 */

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the no. of rows:: ");
        int rows = Integer.parseInt(br.readLine());

        System.out.println();

        int a = 0;
        int b = 1;

        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= i; j++) {

                System.out.print("\t" + a);

              // 1st Approach
              /*   int c = a + b;
                a = b;
                b = c;
             */

            //2ns Approach
               a = b-a;
               b = a+b;
            }
            System.out.println();
        }

        System.out.println();
    }
}
