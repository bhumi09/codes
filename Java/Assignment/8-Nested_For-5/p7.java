/*
  Take the number of rows from user.

       Row = 5

       O
       14   13
       L    K    J
       9    8    7    6
       E    D    C    B    A


       Row = 4

       10
       I    H
       7    6    5
       D    C    B     A
 */

import java.io.*;

class solution {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("\t Enter the no. of rows:: ");
        int rows = Integer.parseInt(br.readLine());

        System.out.println();

        int no = (rows * rows + rows) / 2;
        char ch1 = (char) (64 + no);

        for (int i = 1; i <= rows; i++) {

            if (rows % 2 != 0) {

                for (int j = 1; j <= i; j++) {

                    if (i % 2 != 0) {

                        System.out.print("\t" + ch1);
                    } else {
                        System.out.print("\t" + no);
                    }
                    ch1--;
                    no--;
                }
            } else {

                for (int j = 1; j <= i; j++) {

                    if (i % 2 != 0) {

                        System.out.print("\t" + no);
                    } else {

                        System.out.print("\t" + ch1);
                    }
                    ch1--;
                    no--;
                }
                //System.out.println();
            }
            System.out.println();
        }
        System.out.println();
    }
    
}
