
/* 
    Write a program to take size of array from user and also take integer elements from user.
    Find the Maximum element from the array.

        Input::
            Enter size = 5
            Enter array elements:: 1, 2, 5, 0, 4

        Output::
           Maximum element = 5
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        int max= arr[0];

        for(int i = 0; i < arr.length; i++){
            
            if(arr[i] > max){

                max = arr[i];
               
            }
        
        }
        System.out.println("\n" + "Maximum element = " + max + "\n");  
     
    }
 }