/*
    Write a program to create an array of 'n' integer elements.
    Where 'n' value should be taken from the user.
    Insert the values from users and find the sum of all elements in the array.

        Input::
            n = 6
            Enter elements in the array:: 2, 3, 6, 9, 5, 1
        
        Output:: 26
 
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int sum = 0;

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
                sum = sum + arr[i];
        
        }

        System.out.println("\n" + "Sum of array elements= " + sum + "\n");
    }
 }