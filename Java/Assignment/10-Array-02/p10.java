
/* 
    Write a program to print the elements whose addiion of digits is even
    EX. 26 = 2 + 6 = 8  ( 8 is even so print 26)

        Input::
            Enter array::  [1, 2, 3, 5, 15, 16, 14, 28, 17, 29, 123]
    
        Output::
           2, 15, 28, 17, 123
    
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());
         
        }

        System.out.println("Addition of digits which are even::  ");

       
        for(int i = 0; i < arr.length; i++){

             int sum = 0;
            for(int j = arr[i]; j != 0; j = j/10){

                int rem = j % 10;
                sum = sum + rem;
            }

            if(sum % 2 == 0){

                System.out.println(arr[i]);
            }
        }
        System.out.println();

    }
}
     

                 

 


                         


                     

