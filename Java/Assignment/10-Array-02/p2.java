/* 
    Write a program to find the number of even and odd integers in a given array of integers.

        Input::
            Enter array elements:: 1, 2, 5, 4, 6, 7, 8

        Output::
            Number of even elements:: 4
            Number of odd elements:: 3
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int cnt1 = 0;
        int cnt2 = 0;


        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
            if(arr[i] % 2 == 0){

                cnt1++;
            }
            else{

                cnt2++;
            }
        
        }

        System.out.println("\n" + "Number of even elements= " + cnt1);
        System.out.println("Number of odd elements= " + cnt2 + "\n");
    }
 }