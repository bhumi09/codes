
/* 
    Write a program to search specific element from an array and return its index.

        Input::
            Enter array elements:: 1, 2, 4, 5, 6

            Enter element to search:: 4

        Output::
           Element found at index:: 2
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        System.out.print("Enter element to search::  ");
        int search = Integer.parseInt(br.readLine());

        int index = -1;

        for(int i = 0; i < arr.length; i++){
            
            if(arr[i] == search){

                index = i;
                break;
            }
        }

        if(index == -1){

            System.out.println("\n" + "Element not present..." + "\n");
        }
        else{
            
            System.out.println("\n" + "Eleemnt found at index = " + index + "\n");
        }
    }
 }