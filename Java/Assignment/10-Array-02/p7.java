
/* 
    Write a program to find common elements between two arrays.

        Input::
            Enter first array::  1, 2, 3, 5
            Enter second array::  2, 1, 9, 8

        Output::
            Common element = 1  2
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr1[] = new int[size];
        int arr2[] = new int[size];

        System.out.println("\n" + "Enter  first array elements:: ");
        for(int i = 0; i < arr1.length; i++){

            arr1[i] = Integer.parseInt(br.readLine());
         
        }

        System.out.println("\n" + "Enter second array elements:: ");
        for(int i = 0; i < arr2.length; i++){

            arr2[i] = Integer.parseInt(br.readLine());
        }

        System.out.println("\n" + "Common elements:: ");
       
        for(int i = 0; i < arr1.length; i++){
            for(int j =0; j <arr2.length; j++){

                 if(arr1[i] == arr2[j]){

                       System.out.println(arr1[i]);
               
                  }
           }
        
        }
        System.out.println("\n");
    }
}
     

                 

 


                         


                     

