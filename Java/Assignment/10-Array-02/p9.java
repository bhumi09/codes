
/* 
    Write a program to merge two given arrays.

        Input::
            Enter first array::  [10, 20, 30, 40, 50]
            Enter second array::  [9, 18, 27, 36 45]

        Output::
            Merge Array = [10, 20, 30, 40, 50, 9, 18, 27, 36 45 ]

        Hint:: You can take 3rd array
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of first array::  ");
        int size1 = Integer.parseInt(br.readLine());

        int arr1[] = new int[size1];

        System.out.println("\n" + "Enter  first array elements:: ");
        for(int i = 0; i < arr1.length; i++){

            arr1[i] = Integer.parseInt(br.readLine());
         
        }
        
        System.out.print("\n" + "Enter the size of second array::  ");
        int size2 = Integer.parseInt(br.readLine());

        int arr2[] = new int[size2];

        System.out.println("\n" + "Enter second array elements:: ");
        for(int i = 0; i < arr2.length; i++){

            arr2[i] = Integer.parseInt(br.readLine());
        }

        int arr3[] = new int[size1 + size2];

        for(int i = 0; i< arr1.length; i++){
        
            arr3[i] = arr1[i];
            //System.out.println(arr3[i]);
        }

        int temp = arr1.length;
        for(int i = 0; i< arr2.length; i++){
            
            arr3[temp++] = arr2[i];
           
        }


        System.out.println("\n" + "Merge Array:: ");
        for(int i = 0; i< arr3.length; i++){
            
             System.out.println(arr3[i]);
        }
    }
}
     

                 

 


                         


                     

