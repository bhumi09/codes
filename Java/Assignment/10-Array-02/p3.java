
/* 
    Write a program to find the sum of even and odd numbers in an array.
    Display the sum value.

        Input::
            Enter array elements:: 11, 12, 13, 14, 15

        Output::
            Odd numbers sum:: 39
            Even numbers sum:: 26
 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int EvenSum = 0;
        int OddSum = 0;


        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
            if(arr[i] % 2 == 0){

                EvenSum = EvenSum + arr[i];   
            }
            else{

                OddSum = OddSum + arr[i];
                
            }
        
        }

        System.out.println("\n" + "Sum of even elements= " + EvenSum);
        System.out.println("Sum of odd elements= " + OddSum + "\n");
    }
 }