/* 
   Calculate profit or loss.
   Write a program tthat takes the cost price and selling price (take it hardcoded) and calculate its profit or loss.
   	
   	Input :: selling_price = 1200
		 cost_price = 1000
	Output:: profit of 200

	
   	Input :: selling_price = 300
		 cost_price = 500
	Output:: loss of 200


   	Input :: selling_price = 900
		 cost_price = 900
	Output:: Not a profit or loss
*/

class Solution{

	public static void main(String[] args){

		float selling_price = 300f;
		float cost_price = 500f;

		float price = selling_price - cost_price;

		if(price > 0){

			System.out.println(" profit of "+ price);
		}

		else if(price < 0){

			System.out.println(" loss of "+ price);
		}

		else{

			System.out.println("Not profit not loss");
		}
	}
}


