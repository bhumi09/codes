/* 
   Write a program to find a maximum between three numbers.
   	
   	Input ::  no1 = 1
	 	  no2 = 2
		  no3 = 3

	Output :: 3 is the maximum between 1, 2, and 3.

*/

class Solution {

	public static void main(String[] args){

		int no1 = 1;
		int no2 = 2;
		int no3 = 3;

		if(no1 > no2 && no1 > no3){

			System.out.println(no1 + " is greater than "+ no2 + " and " + no3);
		}
		
		else if(no2 > no1 && no2 > no3){

			System.out.println(no2 + " is greater than "+ no1+ " and " + no3);
		}

		else if(no3 > no1 && no3 > no2){

			System.out.println(no3 + " is greater than "+ no1+ " and " + no2);
		}

		else if(no1 == no2 && no1 > no3){

			System.out.println(no1 + " is greater than "+ no2 + " and " + no3);
		}

		else if(no1 == no2 && no2 > no3){

			System.out.println(no2 + " is greater than "+ no1+ " and " + no3);
		}

		else if(no1 == no2 && no1 < no3){

			System.out.println(no3 + " is greater than "+ no1+ " and " + no2);
		}

		else if(no1 == no2 && no2 < no3){

			System.out.println(no3 + " is greater than "+ no1+ " and " + no2);
		}

		else if(no1 == no3 && no1 > no2){

			System.out.println(no1 + " is greater than "+ no2+ " and " + no3);
		}





	}
}

