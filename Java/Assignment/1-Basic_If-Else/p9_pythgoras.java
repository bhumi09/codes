/* 
   Write a java program, to accept theree numbers and check whether they are pythagoras triplets or not.

   Example:: (what is a pythagoras triplet):

   a = 3, b = 4, c = 5;
   
   if
   a*a + b*b = c*c
   then
   its pythagoras triplet.
   else
   not a pythagoras triplet.

   	Input ::
  	 a = 3, b = 4, c = 5;
	Output = its  a pythagoras triplet.

	Input ::
	a = 1, b = 6, c = 9;
	Output = not a pythagoras triplet.

*/

class Demo{

	public static void main(String[] args){

		int a = 3;
		int b = 4;
		int c = 5;

		if(a*a+b*b == c*c){

			System.out.println("its a pythagoras triplet.");
		}

		else{

			System.out.println("not a pythagoras triplet.");
		}
	}
}


