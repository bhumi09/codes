/* 
   Write a java program, take a number and print whether it is positive or negative.

   	Input = 5
	Output = 5 is positive.

   	Input = -9
	Output = -9 is negative.

	Input = 0
	Output = 0 is neutral.
*/

class Solution{

	public static void main(String[] args){

		System.out.println();

		int no = -9;

		if(no < 0){

			System.out.println(no + " is negative.");
		}
		else if(no > 0){

			System.out.println(no + " is positive.");
		}
		else{

			System.out.println(no + " is neutral");
		}
		System.out.println();
	}
}


