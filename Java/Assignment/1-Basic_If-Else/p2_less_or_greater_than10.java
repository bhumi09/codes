/* 
   Write a java program, take a number and print whether it is less than 10 or greater than 10.

   	Input = 5
	Output = 5is less than 10.

   	Input = 16
	Output = 5 is greater than 10.
*/

class Demo{

	public static void main(String[] args){

		System.out.println();

		int no = 0;

		if(no < 0){

			System.out.println(no + " is less than 10.");
		}
		else if(no > 0){

			System.out.println(no + " is greater than 10.");
		}
		else{

			System.out.println(no + " is neutral");
		}
		System.out.println();
	}
}


