/* 
   Write a java program, that checks a number from 0 to 5 and prints its spelling, if the number is greatre than 5 then print number is greater than 5.

   	Input = 4
	Output = four

   	Input = 6
	Output = 6 is greater than 5.

	Input = -6
	Output = Invalid input.
*/

class Demo{

	public static void main(String[] args){

		int no = 1;

		if(no == 0){

			System.out.println(" Zero ");
		}
		
		else if(no == 1){
	
			System.out.println(" One");
		}
		
		else if(no == 2){

			System.out.println(" Two ");
		}
		
		else if(no == 3){

			System.out.println(" Three ");
		}
		
		else if(no == 4){

			System.out.println(" Four ");
		}
		
		else if(no == 5){

			System.out.println(" Five ");
		}

		else if(no > 5){

			System.out.println(no + " is greater than 5.");
		}
		
		else{
			System.out.println(" Invalid input");
		}
	}
}


