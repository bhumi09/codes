/*
   Write a java program to check if a number is even or odd.
   
   	Input = 10;
	Output = 10 is an even no.

   	Input = 37;
	Output = 37 is odd no.
*/

class Demo{

	public static void main(String[] args){

		System.out.println();
		int no = 10;

		if(no % 2 == 0){

			System.out.println(no + " is an even number.");
		}
		else{
		
			System.out.println(no + " is odd number.");
		}
		System.out.println();
	}
}
