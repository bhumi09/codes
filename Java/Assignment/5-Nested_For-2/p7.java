/*

       1    2     9
       4    25    6
       49   8    81

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println(); 
        int num = 1;

        for(int i =1; i <= 3; i++){
            
            for(int j = 1; j <= 3; j++){
                
                if(num % 2 != 0){

                    System.out.print("\t" + num * num);
                }
                else{
                    System.out.print("\t" + num);
                }
                num++;
            }

              System.out.println();
        }
        System.out.println();
    }
 }





