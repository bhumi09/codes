/*

    C2W1   C2W2   C2W3
    C2W1   C2W2   C2W3
    C2W1   C2W2   C2W3

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        for(int i =1; i <= 3; i++){

            int num = 1;

            for(int j = 1; j <= 3; j++){

                System.out.print("\t" + "C2W");
                System.out.print(num);
                num++;
            }
            System.out.println();
        }
        System.out.println();
    }
 }
