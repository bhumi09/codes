/*
         
    F     5     D     3     B     1
    F     5     D     3     B     1
    F     5     D     3     B     1
    F     5     D     3     B     1
    F     5     D     3     B     1
    F     5     D     3     B     1

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();

        for(int i =1; i <= 6; i++){

            int num  = 5;
            char ch = 'F';

            for(int j = 1; j <= 6; j++){

               if(j % 2 == 1){

                System.out.print("\t" + ch);
                ch -= 2;
               }
               else{
                System.out.print("\t" +  num);
                num -= 2;
               }
    
            }
            System.out.println();
        }
        System.out.println();
    }
 }

