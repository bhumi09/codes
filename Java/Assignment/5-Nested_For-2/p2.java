/*

   4    4    4    4
   5    5    5    5
   6    6    6    6
   7    7    7    7

   */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
       
        int num = 4;
       
        for(int i =1; i <= 4; i++){

            for(int j = 1; j <= 4; j++){

                System.out.print("\t" + num);
            }
            num++;

            System.out.println();
        }
        System.out.println();
    }
 }

