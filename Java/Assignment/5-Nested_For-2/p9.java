/*
         
     1C3     4B2     9A1
     16C3    25B2    36A1
     49C3    64B2    81A1


 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        int num1 = 1;

        for(int i =1; i <= 3; i++){

            int num2 = 3;
            char ch = 'C';

            for(int j = 1; j <= 3; j++){

                System.out.print("\t" + num1 * num1);
                System.out.print(ch--);
                System.out.print(num2-- + "   ");
                num1++;
    
            }
            System.out.println();
        }
        System.out.println();
    }
 }
