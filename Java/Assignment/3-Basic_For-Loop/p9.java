
/*  
   write a program to print sum of first 10 numbers.
*/

class solution{

	public static void main(String[] args){

		int no = 10;
        int sum = 0;

		for(int i = 1; i <= no; i++){

            sum = sum + i;
        }
        System.out.println("\n Sum =   " + sum + "\n" );
	}
}


