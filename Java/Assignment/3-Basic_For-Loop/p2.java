/*  
   write a program to print the first 100 numbers.
*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 100;

		for(int i = 1; i <= no; i++){

			System.out.print( i + "	  " );
	
    	}

        System.out.println();
	}
}
