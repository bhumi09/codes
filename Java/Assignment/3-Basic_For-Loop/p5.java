/*  
write a program to print the odd numbers from 1 - 50 numbers.
*/

class solution{

 public static void main(String[] args){

     int no = 50;

     for(int i = 1; i <= no; i++){
         if(i % 2 != 0){
         
             System.out.print( i + " " );
         }
 
     }

     System.out.println();
 }
}
