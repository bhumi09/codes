
/*  
   write a program to print table of 12 in reverse order.
*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 12;

		for(int i = 10; i >= 1; i--){

			System.out.println(no *  i + "   " );
	
    	}
        System.out.println();
	}
}

