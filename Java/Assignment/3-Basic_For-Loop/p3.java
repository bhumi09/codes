/*  
   write a program to print the first ten 3 digit numbers.
*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 109;

		for(int i = 100; i <= no; i++){

			System.out.print( i + " " );
	
    	}

        System.out.println();
	}
}
