
/*  
   write a program to print reverse from 100 - 1.
*/

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 1;

		for(int i = 100; i >= no; i--){

			System.out.print( i + "     " );
	
    	}
        System.out.println();
	}
}
