/*  
   write a program to print the first 10 numbers.
   */

class solution{

	public static void main(String[] args){

		System.out.println();
		int no = 10;

		for(int i = 1; i <= no; i++){

			System.out.println( i );
		}
		System.out.println();
	}
}
