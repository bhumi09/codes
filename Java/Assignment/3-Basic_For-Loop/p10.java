
/*  
   write a program to print product of first 10 numbers.
*/

class solution{

	public static void main(String[] args){

		int no = 10;
        int prod = 1;

		for(int i = 1; i <= no; i++){

            prod = prod * i;
        }
        System.out.println("\n Product =   " + prod + "\n");
	}
}


