
/*
      F
      E    F
      D    E    F
      C    D    E    F
      B    C    D    E    F
      A    B    C    D    E    F
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 6;
        char ch = (char)(64+rows);
        
        for(int i =1; i <= rows; i++){

            char ch1 = ch;

            for(int j = 1; j <= i; j++){

                System.out.print("\t" + ch1);
                ch1++;
            }
            ch--;
            System.out.println();
        }
        System.out.println();
    }
 }


