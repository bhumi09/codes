
/*
         J
         I     H
         G     F     E
         D     C     B     A
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        char ch = 'J';
        
        for(int i =1; i <= rows; i++){
            
            for(int j = 1; j <= i; j++){

                System.out.print("\t " + ch);
                ch--;
            }
           
            System.out.println();
        }
        System.out.println();
    }
 }


