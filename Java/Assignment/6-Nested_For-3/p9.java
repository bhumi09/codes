
/*
    1 
    8     27
    64    125     216
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 3;
        int num = 1;
        
        for(int i =1; i <= rows; i++){
            
            for(int j = 1; j <= i; j++){

                System.out.print("\t" + num*num*num);
                num++;
            }
           
            System.out.println();
        }
        System.out.println();
    }
 }


