
/*

    C2W
    C2W   C2W
    C2W   C2W   C2W
    C2W   C2W   C2W   C2W

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        for(int i =1; i <= 4; i++){

            for(int j = 1; j <= i; j++){

                System.out.print("\t" + "C2W");
            }
            System.out.println();
        }
        System.out.println();
    }
 }
