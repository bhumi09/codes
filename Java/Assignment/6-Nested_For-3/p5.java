
/*

        10    10    10    10
        11    11    11
        12    12 
        13
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        int num = 10;
        
        for(int i =1; i <= rows; i++){

            for(int j = 1; j <= rows-i+1; j++){

                System.out.print("\t" + num);
            }
            num++;
            System.out.println();
        }
        System.out.println();
    }
 }

