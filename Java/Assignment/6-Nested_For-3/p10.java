
/*

       1    2     3     4
       4    5     6
       6    7
       7
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        int num = 1;
        
        for(int i =1; i <= rows; i++){

            //num = i;
 
            for(int j = 1; j <= rows-i+1; j++){

                System.out.print("\t" + num);
                num++;
            }
            num--;
            System.out.println();
        }
        System.out.println();
    }
 }


