
/*
         9
         9    8
         9    8     7
         9    8     7     6
 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        int rows = 4;
        
        for(int i =1; i <= rows; i++){
            
            int num = 9;

            for(int j = 1; j <= i; j++){

                System.out.print("\t" + num);
                num--;
            }
           
            System.out.println();
        }
        System.out.println();
    }
 }


