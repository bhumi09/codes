
/* 
    Write a program to find Palindrome number from an array return its index. 
    Take size and element from the user.

        Input::
            Enter array elements:: 10, 25, 252, 36, 564

        Output::
          Palindrome no. 252 found at index 2.
 */

 import java.io.*;
 class solution{

    void FindPalindrome(int arr[]){

        for(int i = 0; i < arr.length; i++){
            
            int rev = 0;

            for(int j = arr[i]; j != 0; j = j / 10){

                int rem = j % 10;
                rev = rev * 10 + rem;
                
            }
            if(rev == arr[i]){

                System.out.println("Palindrome no " + arr[i] + " found at index " + i + "\n");
            }   
        }
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.FindPalindrome(arr);

        br.close();
    }
 }