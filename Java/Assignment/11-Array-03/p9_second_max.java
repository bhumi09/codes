
/* 
    Write a program to print the second max element in the array . 
    Take size and element from the user.

        Input::
            Enter array elements:: 2, 255, 2, 1554, 15, 65

        Output:: 255
 */

 import java.io.*;
 class solution{

    void SecondMax(int arr[]){

        int max = arr[0];
		
        for(int i=0; i<arr.length; i++){
			
            if(arr[i]>max){
				max = arr[i];
			}
		}
		int secMax = arr[0];
		
        for(int i=0; i<arr.length; i++){
			
            if(arr[i]>secMax && arr[i] != max){
				secMax = arr[i];
			}
		}
		System.out.print("Second maximum element::  " + secMax);
		System.out.println();

        }


    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.SecondMax(arr);

        br.close();
    }
}


 