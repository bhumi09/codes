

/* 
    Write a program to print the second min element in the array . 
    Take size and element from the user.

        Input::
            Enter array elements:: 2, 255, 95, 1554, 15, 65, 89

        Output:: 15
 */

 import java.io.*;
 class solution{

    void SecondMin(int arr[]){

        int min = arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]<min){
				min = arr[i];
			}
		}
		int secMin = arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]<secMin && arr[i] != min){
				secMin = arr[i];
			}
		}
		System.out.print("Second minimum element::  " + secMin);
		System.out.println();
	}

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.SecondMin(arr);

        br.close();
    }
}


 