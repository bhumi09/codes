
/* 
    Write a program to find Perfect number from an array return its index. 
    Take size and element from the user.

        Input::
            Enter array elements:: 10, 25, 252, 496, 564

        Output::
          Perfect no. 496 found at index 3.
 */

 import java.io.*;
 class solution{

    void FindPerfect(int arr[]){

        for(int i = 0; i < arr.length; i++){

            int sum = 0;
            for(int j = 1; j < arr[i]; j++){

                if(arr[i] % j == 0){
                    
                    sum = sum + j;
                }
            }
            if(sum == arr[i]){

                System.out.println("Perfect no " + arr[i] + " found at index " + i + "\n");
            }   
        }
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.FindPerfect(arr);

        br.close();
    }
 }