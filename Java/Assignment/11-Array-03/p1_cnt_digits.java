
/* 
    Write a program to print count of digits in elements of array.
        Input::
            Enter array elements:: 02, 255, 2, 1554

        Output::
            Count of digits = 2, 3, 1, 4
 */

 import java.io.*;
 class solution{

    void CountDigits(int arr[]){

        System.out.println("\n Count of digits:: ");


        for(int i = 0; i < arr.length; i++){

            int cnt = 0;
            for(int j = arr[i]; j != 0; j = j / 10){

                cnt++;
            }

            System.out.println(arr[i] + "\t=>  " + cnt);
            
        }

        
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.CountDigits(arr);

       
    }
 }