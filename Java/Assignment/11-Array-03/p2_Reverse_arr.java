/* 
    Write a program to reverse each element in an array. 
    Take size and element from the user.

        Input::
            Enter array elements:: 01, 25, 252, 36, 564

        Output::
           Reverse array::  10, 52, 252, 63, 465
 */

 import java.io.*;
 class solution{

    void ReverseArr(int arr[]){

        for(int i = 0; i < arr.length; i++){

            int rev = 0;

            for(int j = arr[i]; j != 0; j = j / 10){

                int rem = j % 10;
                rev = rev * 10 + rem;
            }
            arr[i] = rev;       
        }     
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.ReverseArr(arr);

        System.out.println("\n Reverse Array elements:: ");
        for(int i = 0; i < arr.length; i++){

            System.out.println("   " + arr[i]);
        }
    }
 }