
/* 
    Write a program to find composite number from an array return its index. 
    Take size and element from the user.

        Input::
            Enter array elements:: 1, 2, 3, 5, 6, 7

        Output::
          Composite no. 6 found at index 4.
 */

 import java.io.*;
 class solution{

    void FindComposite(int arr[]){

        for(int i = 0; i < arr.length; i++){

            int cnt = 0;
            for(int j = 1; j <= arr[i]; j++){

                if(arr[i] % j == 0){
                    cnt++;
                }
                if(cnt > 2){

                    break;
                }
            }
            if(cnt > 2){

                System.out.println("Composite " + arr[i] + " found at index " + i + "\n");
            }   
        }
    }

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        solution s1 = new solution();

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        s1.FindComposite(arr);

        br.close();
    }
 }