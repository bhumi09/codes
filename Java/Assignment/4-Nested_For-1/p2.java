/*

     4   5   6   7
     4   5   6   7
     4   5   6   7
     4   5   6   7

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println();
        
        for(int i =1; i <= 4; i++){

            int no = 4; 
            
            for(int j = 1; j <= 4; j++){

                System.out.print(no + "  ");
                no++;
            }
            System.out.println();
        }
        System.out.println();
    }
 }