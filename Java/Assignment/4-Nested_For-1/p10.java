/*

       1    2    3    4
       2    3    4    5
       3    4    5    6
       4    5    6    7

 */

 class solution{

    public static void main(String[] args) {
        
        System.out.println(); 
        int num = 1;

        for(int i =1; i <= 4; i++){
            
            num = i;

            for(int j = 1; j <= 4; j++){

                System.out.print("\t" + num); 
                num++;           
            }

              System.out.println();
        }
        System.out.println();
    }
 }





