/* 
   Write a program to print a table of 2.
   	
   	output :: 2   4    6   8   10   12   14   16   18   20

*/

class solution{

	public static void main(String[] args){

		System.out.println();

		int i = 1;
		System.out.println("Table of 2 ::    ");
		
		while( i <= 10){

			System.out.print((i * 2) + "  ");
			i++;
		}

		System.out.println("\n");
	}
}
