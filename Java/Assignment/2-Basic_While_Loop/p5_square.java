/* 
 
   Write a program to print the square of even digits of a given number.

   	input:: 942111423
	output :: 4   16   4   16

*/

class solution{

	public static void main(String[] args){

		int no = 942111423;

		while(no != 0){

			int rem = no % 10;
			no = no / 10;

			if(rem % 2 == 0){

				System.out.println(rem * rem);
			}
		}

	}
}
