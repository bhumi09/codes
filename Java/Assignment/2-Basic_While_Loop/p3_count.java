/* 
 
   Write a program to count the digits of a given number.

   	input:: 942111423
	output :: count of digits = 9

*/

class solution{

	public static void main(String[] args){

		int no = 942111423;
		int count = 0;

		while(no != 0){

			int rem = no % 10;
			no = no / 10;
			count++;
		}

		System.out.println("\n Count of digits is " + count  + "\n");
	}
}
