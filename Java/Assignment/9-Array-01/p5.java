/*
    Write a program to  take 10 input from user.
    Print only elements that are divisible by 5.

        Input:: 
            Enter array elements:: 10, 2, 2, 3, 3, 3, 4, 4, 25, 55
        
        Output:: 
            10, 25, 55

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("\n" + "Enter array elements:: ");

        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i< arr.length; i++){
            
            if(arr[i] % 5 == 0){

                System.out.print("  " + arr[i]);
            }
        }

        System.out.println();
    }
 }