/*
    Write a program to  take size of array from user and also take the integer elements from user.
    Print sum of odd elements only.

        Input:: 
            Enter size:: 5
            Enter array elements:: 1, 2, 3, 4, 5
        
        Output:: 9
            //1 + 3 + 5 = 9

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int sum = 0;

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
            if(arr[i] % 2 == 1){

                sum = sum + arr[i];
            }
        }

        System.out.println("\n" + "Sum of odd elements= " + sum + "\n");
    }
 }