/*
    Write a program to  take size of array from user and also take the integer elements from user.
    Print Product of even elements only.

        Input:: 
            Enter size:: 9
            Enter array elements:: 1, 2, 3, 2, 5, 10, 55, 77, 99
        
        Output:: 40
            //2 * 2 * 10

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int Product = 1;

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());
        }

        for(int i = 0; i < arr.length; i++){
            
            if(arr[i] % 2 == 0){

                Product = Product * arr[i];
            }
        }

        System.out.println("\n" + "Product of even elements= " + Product + "\n");
    }
 }