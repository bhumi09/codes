/*
    Write a program to  take size of array from user and also take the integer elements from user.
    Print product of odd index only.

        Input:: 
            Enter size:: 6
            Enter array elements:: 1, 2, 3, 4, 5, 6
        
        Output:: 48
            //2 * 4 * 6

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        int Product = 1;

        System.out.println("\n" + "Enter array elements:: ");
        for(int i = 0; i < arr.length; i++){

            arr[i] = Integer.parseInt(br.readLine());

        }

        for(int i = 0; i < arr.length; i++){
            
            if(i % 2 == 1){

                Product = Product * arr[i];
            }

        }

        System.out.println("\n" + "Product of odd index= " + Product + "\n");
    }
 }