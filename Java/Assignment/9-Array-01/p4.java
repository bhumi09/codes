/*
    Write a program to  take size of array from user and also take the integer elements from user.
    Print only vowels from the array.

        Input:: 
               a, b, c, o, d, p, e
        
        Output:: 
            a, o, e

 */

 import java.io.*;
 class solution{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();

        System.out.print("Enter the size of array::  ");
        int size = Integer.parseInt(br.readLine());

        char arr[] = new char[size];

        System.out.println("\n" + "Enter array elements:: ");
        
        for(int i = 0; i < arr.length; i++){

            arr[i] = (char)br.read();
           br.readLine();

        }

        int flag = 0;

        for(int i = 0; i< arr.length; i++){

            if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u'||
               arr[i] == 'A' || arr[i] == 'E' || arr[i] == 'I' || arr[i] == 'O' || arr[i] == 'U' ){

                System.out.print("  " + arr[i]);
                flag = 1;
            }

        }

        if(flag == 0) {

            System.out.println("No vowels are present...");
        }

        System.out.println();
    }
 }